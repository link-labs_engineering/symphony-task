/*****************************************************************************/
/*****INCLUDES****************************************************************/
//-----Standard Libraries-----//
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//-----My Libraries-----//
#include "bsp_nrf.h"

#undef NRF_LOG_MODULE_NAME
#define NRF_LOG_MODULE_NAME symphony_task
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#include "watchdog_task.h"

#include "ll_ifc.h"
#include "ll_ifc_ftp.h"
#include "ll_ifc_no_mac.h"
#include "ll_ifc_symphony.h"
#include "symphony_module.h"
#include "symphony_task.h"
#include "nrf_drv_rng.h"

//-----FreeRTOS Libraries-----//
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "task_mgmt.h"

/*****************************************************************************/
/*****DEFINES*****************************************************************/
//-----Symphony Task Configuration-----//
// max number of sequential errors trying to connect to a gateway
#define GW_CONNECT_MAX_ERRS 2
// max number of sequential errors with ll_ifc before resetting module
#define IFC_MAX_ERRS 10
// max number of resets before completely clearing flash variables
#define IFC_CLEAR_FLASH 3
// rate symphony task runs at when activity is occurring (or expected)
#define SYM_ACTIVE_TASK_RATE_MS 1000
// rate symphony task runs at when FOTA is occurring
#define SYM_FOTA_TASK_RATE_MS 250
// time to hold the RESET line
#define SYM_RESET_LOW_MS 1000
// delay after resetting module before attempting communications
#define SYM_POST_RESET_DELAY_MS 7000
// delay after issuing delete settings command
#define SYM_POST_DELETE_DELAY_MS 6000
// max length of time to go without checking module status
#define SYM_MIN_STATUS_UPDATE_RATE_MS 5000
// time to wait after scan before going to NOT_READY state when device reports DISCONNECTED
#define SYM_DISCONNECTED_TIMEOUT_MS 180000
// time to wait for an Info scan to complete before going to NOT_READY
#define SYM_INFO_SCAN_TIMEOUT_MS 600000
// time to wait after scan before going to NOT_READY state when device reports INITIALIZING
#define SYM_INITIALIZING_TIMEOUT_MS 120000
// timeout until TX_SUCCESS or TX_ERROR is received when sending a message
#define SYM_TX_TIMEOUT_MS 120000
// max number of consecutive tx errors before going to not ready state
#define SYM_MAX_TX_FAILURES 5
// length of time after mailbox check before assuming no messages will be received
#define SYM_DL_CHECK_TIMEOUT_MS 30000
// time after FTP apply before ftp_done called
#define SYM_FTP_DONE_TIMEOUT_MS 4000

// if the symphony module fails a scan it needs time to recoup
#define SYM_SETTLE_TIMEOUT_MS 15000

#define SYM_STATE_PRINT_INTERVAL_MS 6000
//-----Debug Prints Alias-----//
#define ENABLE_SYM_DEBUG_PRINTS 1

#if defined(DEBUG_PRINT_ENABLED) && defined(ENABLE_SYM_DEBUG_PRINTS)
#define SYM_DEBUG_PRINT(x) Debug_Print(x)
#define SYM_DEBUG_PRINTF(...) Debug_Printf(__VA_ARGS__)
#else
#define SYM_DEBUG_PRINT(x)
#define SYM_DEBUG_PRINTF(...)
#endif

#define SYM_CMD_QUEUE_SIZE              (8u)
#define SYM_MSG_QUEUE_SIZE              (1u)

#define SYM_SLOW_TASK_RATE_MS           (3000u)
#define SYM_SLOW_TASK_PERIOD_TICKS      (SYM_SLOW_TASK_RATE_MS / portTICK_PERIOD_MS)

//#define SYM_TASK_NO_WATCHDOG
/*****************************************************************************/
/*****TYPEDEFS****************************************************************/
// error return codes
typedef enum { NO_ERR, ERR_NO_RESET, ERR_RESET, ERR_FLAGS} err_check_ret_t;

typedef enum { 
    MSG_STATE_NONE,
    MSG_STATE_WAITING_READY,
    MSG_STATE_TRANSMITTING,
    MSG_STATE_TX_SUCCESS
   } message_state_t;

// tx message struct
#define SYMPHONY_MAX_PAYLOAD_SIZE 256
typedef struct __attribute__((packed)) sym_tx_msg_struct
{
    uint16_t        msg_len;    // length of message to be sent
    uint8_t         msg[SYMPHONY_MAX_PAYLOAD_SIZE];   // message buffer
    bool            ack;        // true if ACK requested (note: xTaskToNotify will be notified when message is sent to
                            // module if ack = false)
    uint8_t         port;       // transmit port
    message_state_t msg_state;
    uint16_t        crc;
} sym_tx_msg_struct_t;

// FTP variables
typedef struct
{
    bool success;
    uint32_t file_id;
    uint32_t file_ver;
    uint32_t file_sz;
} sym_ftp_var_t;

typedef enum {
    HOP_STATE_INIT,
    HOP_STATE_CONNECT
    }hop_states_t;

typedef enum {
    INFO_SCAN_STATE_INIT,
    INFO_SCAN_STATE_WAIT
    }info_scan_states_t;

/*****************************************************************************/
/*****LOCALS******************************************************************/

static const char * s_sym_state_str[NUM_SYM_STATES] = {
    "SYM_RESET",                  
    "SYM_INITIALIZING",           
    "SYM_NOT_READY",            
    "SYM_SCANNING",  
    "SYM_TEST_TX",
    "SYM_READY",   
    "SYM_OFF",      
    "SYM_FOTA",     
    "SYM_INFO_SCANNING",      
    "SYM_HOP_GW",          
    "SYM_STATE_ERROR"    
};

const ll_version_t supportHop_version = {
    .major = 1,
    .minor = 6,
    .tag =0
} ;

// RTOS variables
static xTaskHandle s_LL_symphonyHandle;
static xQueueHandle s_sym_cmd_queue;
//static xQueueHandle s_sym_tx_queue;
// Watchdog handler
static wdg_handler_t s_sym_wdg_handler;
// Symphony variables
static bool s_does_support_hop;
static network_config_t s_sym_net_config;
static sym_task_config_t s_sym_task_config;
static enum ll_state s_sym_state;
static enum ll_tx_state s_tx_state;
static enum ll_rx_state s_rx_state;
static uint32_t s_irq_flags;
// FTP variables
static ll_ftp_t s_ftp;
static ll_ftp_callbacks_t s_ftp_callbacks;
static sym_ftp_config_t s_ftp_config;
static sym_ftp_var_t s_ftp_vars;
// Symphony task variables
static uint32_t s_err_count;
static uint32_t s_gw_err_count;
static uint32_t s_clear_flash_count;
static uint32_t s_connect_int_modifier;
static uint32_t s_tx_fail_count;
static bool s_tx_in_progress;
static bool s_mailbox_check_inhibit;
static bool s_mailbox_check_next;
static sym_module_state_t s_current_state;
static portTickType s_state_entry_tick;
static portTickType s_last_dl_check_tick;
static portTickType s_msg_send_tick;
static portTickType s_status_update_tick;
static portTickType s_last_ready_tick;
static portTickType s_dl_check_timeout_tick;
static portTickType s_fota_cycle_tick;
static portTickType s_gw_hop_tick;
static portTickType s_sym_print_tick;

static uint8_t s_dl_msg_bfr[256];
static uint16_t s_dl_msg_len;
static uint16_t s_num_gw_connect_errors;
static bool was_last_gw_error;
static uint16_t s_module_status_error_count;

//static uint8_t s_tx_msg_buff[256];

static ll_version_t s_sym_version;
static uint64_t     s_mod_uuid = 0ull;

static sym_tx_msg_struct_t __attribute__((section(".non_init"))) s_sym_tx_buff;

static hop_states_t hop_state = HOP_STATE_INIT;
static info_scan_states_t  i_scan_state = INFO_SCAN_STATE_INIT;

static llabs_time_t    s_sym_net_time =
{
    .seconds = 0,
    .millis = 0
};

/*****************************************************************************/
/*****PRIVATE FUNCTION PROTOTYPES*********************************************/
// RTOS tasks
static void _symphony_task(void const *argument);

// state processing functions
static sym_module_state_t _sym_reset_state(void);
static sym_module_state_t _sym_initializing(void);
static sym_module_state_t _sym_module_scanning(void);
static sym_module_state_t _sym_not_ready(void);
static sym_module_state_t _sym_ready(void);
static sym_module_state_t _sym_off(void);
static sym_module_state_t _sym_fota(void);
static sym_module_state_t _sym_module_hop_gw(void);
static sym_module_state_t _sym_module_info_scanning(void);

// utility functions
static void _sym_downlink_check(void);
static err_check_ret_t _sym_ll_ifc_error_check(int32_t ifc_ret);
static err_check_ret_t _sym_get_module_status(bool force);
static sym_tx_state_t _sym_process_tx_state(void);
static void _sym_process_rx_state(void);
static sym_module_state_t _sym_process_cmd_queue(sym_module_state_t state_in);
static uint32_t _sym_time_in_state_ms(void);
static uint32_t _sym_time_to_now_ms(TickType_t t0);
static uint32_t _sym_get_connection_int_ms(void);
static void _sym_inc_connection_modifier(void);
static void _sym_flush_tx_queue(void);
static portTickType _sym_get_task_delay(void);
static void _sym_long_delay(uint32_t total_delay);

// callback convenience functions
static inline bool _get_io0_state(void);
static inline void _set_power(sym_power_t state);
static inline void _set_reset(sym_reset_t state);
static inline void _set_boot(sym_boot_t state);
static inline void _on_rx_done(uint8_t *msg, uint16_t len, uint8_t port);
static inline void _on_suspend(void);
static inline void _on_resume(void);
static inline void _on_state_change(sym_module_state_t new_state, sym_module_state_t old_state);
static inline void _on_error(sym_err_code_t err_code);
static inline void _on_tx_done(sym_tx_state_t tx_state);

// LL FTP functions & callbacks
static void _sym_cycle_ftp(void);
static ll_ftp_return_code_t _ftp_open_callback(uint32_t file_id, uint32_t file_version,
                                               uint32_t file_size);
static ll_ftp_return_code_t _ftp_read_callback(uint32_t file_id, uint32_t file_version,
                                               uint32_t offset, uint8_t *payload, uint16_t len);
static ll_ftp_return_code_t _ftp_write_callback(uint32_t file_id, uint32_t file_version,
                                                uint32_t offset, uint8_t *payload, uint16_t len);
static ll_ftp_return_code_t _ftp_close_callback(uint32_t file_id, uint32_t file_version);
static ll_ftp_return_code_t _ftp_apply_callback(uint32_t file_id, uint32_t file_version,
                                                uint32_t file_size);
static ll_ftp_return_code_t _ftp_send_uplink_callback(const uint8_t *buf, uint16_t len, bool acked,
                                                      uint8_t port);
static ll_ftp_return_code_t _ftp_dl_config_callback(bool downlink_on);
/*****************************************************************************/
/*****PUBLIC FUNCTIONS********************************************************/
//uint8_t GenerateRandomNumber()
//{
//   NRF_RNG_VALRDY = 0;
//   NRF_RNG->TASKS_START = 1;
//   while(NRF_RNG->VALRDY == 0){}
//   NRF_RNG->TASKS_STOP = 1;
//   return NRF_RNG->VALUE;
//}

static void s_sym_msg_update_crc(void)
{
    uint16_t ret_crc;

    ret_crc = crc16((uint8_t *)&s_sym_tx_buff, sizeof(sym_tx_msg_struct_t)-2);

    s_sym_tx_buff.crc = ret_crc;
}

static void s_sym_msg_change_state(message_state_t ms)
{
    s_sym_tx_buff.msg_state = ms;
    s_sym_msg_update_crc();
}

static bool s_sym_msg_check_crc(void)
{
    uint16_t ret_crc;
    ret_crc = crc16((uint8_t *)&s_sym_tx_buff, sizeof(sym_tx_msg_struct_t)-2);

    if (ret_crc != s_sym_tx_buff.crc)
    {
        return false;
    }
   
    return true;
}

static void _sym_check_reset_msg_state(void)
{
    if (s_sym_msg_check_crc()==false)
    {
        memset((uint8_t*)&s_sym_tx_buff, 0, sizeof(sym_tx_msg_struct_t)); 
        s_sym_msg_change_state(MSG_STATE_NONE);
    }
    else
    {
        if (s_sym_tx_buff.msg_state != MSG_STATE_NONE)
        {
            s_sym_msg_change_state(MSG_STATE_WAITING_READY);
        }
    }
}

static uint8_t genRandomBetween(uint8_t x, uint8_t y)
{
    uint8_t num_rand_bytes_available;
    uint32_t err_code;
    uint8_t rand_number;
    float result;
    uint8_t lowest_number = x;
    int32_t ret;

    do{
        ret = nrf_drv_rng_rand((uint8_t*)&rand_number, 1);
    }while (ret!= NRF_SUCCESS);

    result = y - x;
    if (result<0)
    {
        result = -result;
        lowest_number = y;
    } 

    rand_number = (uint8_t)(((result * (float)rand_number)/255.0f) + 0.5);  

    return lowest_number + (uint8_t)(rand_number);

}

void sym_task_disable(void)
{
    vTaskSuspend(s_LL_symphonyHandle);
}

int32_t sym_init_task(network_config_t *net_config, sym_task_config_t *task_config,
                      sym_ftp_config_t *ftp_config)
{
    int32_t ret = 0;
    int32_t err_code = 0;
    // init config settings
    s_sym_net_config = *net_config;
    // init task variables
    s_sym_task_config = *task_config;
    s_err_count = 0;
    s_gw_err_count = 0;
    s_clear_flash_count = 0;
    s_connect_int_modifier = 0;
    s_tx_fail_count = 0;
    s_tx_in_progress = false;
    s_current_state = SYM_RESET;
    s_state_entry_tick = xTaskGetTickCount();
    s_last_dl_check_tick = xTaskGetTickCount();
    s_dl_check_timeout_tick = xTaskGetTickCount();
    s_fota_cycle_tick = xTaskGetTickCount();
    s_gw_hop_tick =  xTaskGetTickCount();
    s_sym_print_tick = xTaskGetTickCount();
    s_mailbox_check_inhibit = false;
    s_mailbox_check_next = false;
    s_does_support_hop =  false;
    s_num_gw_connect_errors = 0;

    err_code = nrf_drv_rng_init(NULL);
    APP_ERROR_CHECK(err_code);

    s_module_status_error_count = 0;
    // init ftp
    if (ftp_config != NULL)
    {
        if (ftp_config->ftp_file_open == NULL)
        {
            ret = -1;
        }
        else if (ftp_config->ftp_file_read == NULL)
        {
            ret = -1;
        }
        else if (ftp_config->ftp_file_write == NULL)
        {
            ret = -1;
        }
        else
        {
            s_ftp_config = *ftp_config;

            s_ftp_callbacks.apply = _ftp_apply_callback;
            s_ftp_callbacks.close = _ftp_close_callback;
            s_ftp_callbacks.config = _ftp_dl_config_callback;
            s_ftp_callbacks.open = _ftp_open_callback;
            s_ftp_callbacks.read = _ftp_read_callback;
            s_ftp_callbacks.uplink = _ftp_send_uplink_callback;
            s_ftp_callbacks.write = _ftp_write_callback;

            memset(&s_ftp_vars, 0, sizeof(sym_ftp_var_t));

            if(ll_ftp_init(&s_ftp, &s_ftp_callbacks) != LL_FTP_OK)
            {
                ret = -1;
            }
        }
    }
    err_code = xTaskCreate(
                          (TaskFunction_t) (_symphony_task), 
                          (const char *) "sym_task",
                          SYM_TASK_STACK_SIZE, 
                          NULL, 
                          SYM_TASK_PRIORITY, 
                          &s_LL_symphonyHandle);
                      
    // create tasks
    if (pdPASS != err_code)
    {
     LL_ASSERT(0);   
    }

    // create queues
    s_sym_cmd_queue = xQueueCreate(SYM_CMD_QUEUE_SIZE, sizeof(sym_cmd_t));

    if (NULL == s_sym_cmd_queue)
    {
        LL_ASSERT(0);
    }

    _sym_check_reset_msg_state();

 //   s_sym_tx_queue = xQueueCreate(SYM_MSG_QUEUE_SIZE, sizeof(sym_tx_msg_struct_t));

    // config module pins
    _set_power(SYM_POWER_ON);
    _set_boot(SYM_NORMAL_MODE);
    _set_reset(SYM_NOT_IN_RESET);

    was_last_gw_error = false;
#ifndef SYM_TASK_NO_WATCHDOG
    // Register watchdog
    s_sym_wdg_handler = wdg_task_register("SYM", s_LL_symphonyHandle);
    if(WDG_HANDLER_ERROR == s_sym_wdg_handler)
    {
        ret = -1;
    }
#endif
    return (ret);
}
/*****************************************************************************/
inline ll_version_t sym_get_version(void)
{
    return (s_sym_version);
}
/*****************************************************************************/
inline uint64_t sym_get_uuid(void)
{
    return (s_mod_uuid);
}
/*****************************************************************************/
inline sym_module_state_t sym_get_state(void)
{
    return (s_current_state);
}
/*****************************************************************************/
inline void sym_inhibit_mailbox_check(void)
{
    s_mailbox_check_inhibit = true;
}
/*****************************************************************************/
inline void sym_allow_mailbox_check(void)
{
    s_mailbox_check_inhibit = false;
}
/*****************************************************************************/
int32_t sym_enqueue_cmd(sym_cmd_t cmd_data)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if (xQueueSendToBackFromISR(s_sym_cmd_queue, &cmd_data, 0) == errQUEUE_FULL)
    {
        return (-1);
    }
    else
    {
        vTaskNotifyGiveFromISR(s_LL_symphonyHandle,  &xHigherPriorityTaskWoken );
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
        return (0);
    }
}
/*****************************************************************************/
sym_send_msg_ret_t sym_send_msg(uint8_t *msg, uint16_t msg_len, bool ack, uint8_t port)
{
    //sym_tx_msg_struct_t cmd_data;
    BaseType_t xHigherPriorityTaskWoken;

    if (msg_len == 0)
    {
        return (SYM_NULL_MSG);
    }
    else if (msg_len > 256)
    {
        return (SYM_MSG_TOO_LONG);
    }

    if ((s_current_state == SYM_READY)||(s_current_state == SYM_TEST_TX))
    {
        if (s_sym_tx_buff.msg_state == MSG_STATE_NONE)
        {
            // Copy the data to the msg struct
            memcpy((uint8_t *) s_sym_tx_buff.msg, msg, (size_t) msg_len);
            s_sym_tx_buff.msg_len = msg_len;
            s_sym_tx_buff.ack = ack;
            s_sym_tx_buff.port = port;
            s_sym_tx_buff.msg_state = MSG_STATE_WAITING_READY;

            SYM_DEBUG_PRINTF(
                "sym_send_msg @ %p, len=%d, ack=%d, port=%d",
                s_sym_tx_buff.msg,
                s_sym_tx_buff.msg_len, 
                (int) s_sym_tx_buff.ack,
                s_sym_tx_buff.port
            );
            
            //memcpy((uint8_t*)&s_sym_tx_buff,(uint8_t*)&cmd_data, sizeof(sym_tx_msg_struct_t));
            s_sym_msg_update_crc();

            vTaskNotifyGiveFromISR(s_LL_symphonyHandle, &xHigherPriorityTaskWoken );

            return (SYM_MSG_ENQUEUED);
        }
        else
        {
            SYM_DEBUG_PRINT("s_sym_tx_queue (spaces) is full!");
            return (SYM_MSG_QUEUE_FULL);
        }
    }
    else
    {
        return (SYM_MOD_NOT_READY);
    }
}
static void sym_delete_all_gw(void)
{
   uint8_t i;

   SYM_DEBUG_PRINTF("GHW: delete all gateways"); 

   for (i=0; i<MAX_GW_SCAN_RESULTS; i++)
   {
       s_sym_net_config.gw_list[i].gw_data.is_active = false;
       s_sym_net_config.gw_list[i].gw_data.id = 0;
       s_sym_net_config.gw_list[i].gw_data.channel = -1;
       s_sym_net_config.gw_list[i].gw_data.error_count = 0;
       s_sym_net_config.gw_list[i].gw_data.rssi = 0;
   }  
   
   s_sym_net_config.num_gateways = 0;

   if (s_sym_task_config.sym_gw_list_update!=NULL)
   {
       s_sym_task_config.sym_gw_list_update(&s_sym_net_config);
   }
}

/****************************************************************************
**sym_update_net_config
**  
*/
void sym_update_net_config(network_config_t *net_config, bool update_module)
{
    s_sym_net_config = *net_config;

    if (update_module == true)
    {
        sym_enqueue_cmd(SYM_CMD_RST);
        sym_delete_all_gw();
    }
}

/*****************************************************************************/
void sym_update_scan_config(network_config_t *net_config)
{
    s_sym_net_config = *net_config;
    sym_delete_all_gw();  // if changing scanning info we may want to do a new scan
    sym_enqueue_cmd(SYM_CMD_RST);
}

/*****************************************************************************/
void sym_update_task_config(sym_task_config_t *task_config)
{
    s_sym_task_config = *task_config;
}
/*****************************************************************************/
inline xTaskHandle sym_task_get_handle(void)
{
    return (s_LL_symphonyHandle);
}
/*****************************************************************************/
inline void sym_reset_connection_interval(void)
{
    s_connect_int_modifier = 0;
}
/*****************************************************************************/
inline portTickType sym_get_last_connection_time(void)
{
    return (s_last_ready_tick);
}
/*****************************************************************************/
/*****PRIVATE FUNCTIONS*******************************************************/
static bool is_time_to_print_state(void)
{
    if (_sym_time_to_now_ms(s_sym_print_tick) > SYM_STATE_PRINT_INTERVAL_MS)
    {
        s_sym_print_tick =  xTaskGetTickCount();
        return true;
    }
    return false;
}

static void _symphony_task(void const *argument)
{
    UNUSED_PARAMETER(argument);
    
    sym_module_state_t new_state;
    
    wdg_task_refresh(s_sym_wdg_handler);

    SYM_DEBUG_PRINT("Symphony Task start");
    _on_state_change(s_current_state, s_current_state);
    for (;;)
    {
        // process state
        switch (s_current_state)
        {
            case SYM_RESET:
                new_state = _sym_reset_state();
                break;
            case SYM_INITIALIZING:
                new_state = _sym_initializing();
                break;
            case SYM_SCANNING:
                new_state = _sym_module_scanning();
                break;
            case SYM_NOT_READY:
                new_state = _sym_not_ready();
                break;
            case SYM_TEST_TX:
            case SYM_READY:
                new_state = _sym_ready();
                break;
            case SYM_OFF:
                new_state = _sym_off();
                break;
            case SYM_FOTA:
                new_state = _sym_fota();
                break;
            case SYM_INFO_SCANNING:
               // SYM_DEBUG_PRINT("STATE = INFO_SCAN");
                new_state = _sym_module_info_scanning();
                break;
            case SYM_HOP_GW:
                //SYM_DEBUG_PRINT("STATE = HOP");
                new_state = _sym_module_hop_gw();
                break;
            case SYM_STATE_ERROR:
            default:
                _on_error(SYM_TASK_STATE_ERROR);
                new_state = _sym_reset_state();
                break;
        }
        // downlink handler
        _sym_downlink_check();
        // reset check
        if (s_err_count > IFC_MAX_ERRS)
        {
            new_state = SYM_RESET;
        }
        // time-tag transitions (for error checking later)
        if (s_current_state != new_state)
        {
            SYM_DEBUG_PRINTF("SYM STATE CHANGE %s --> %s", s_sym_state_str[s_current_state], s_sym_state_str[new_state]);
            _on_state_change(new_state, s_current_state);
            s_state_entry_tick = xTaskGetTickCount();
        }

        s_current_state = new_state;

        if (is_time_to_print_state())
        {
            SYM_DEBUG_PRINTF("Sym STATE = %s", s_sym_state_str[s_current_state]);
        }

        // cycle ftp engine
        _sym_cycle_ftp();
        
        // Refesh the watchdog - the following command may block us for the mailbox period
        wdg_task_refresh(s_sym_wdg_handler);

        _on_suspend();
        ulTaskNotifyTake(pdTRUE, _sym_get_task_delay());
        _on_resume();
    }
}
/*****************************************************************************/
static void _sym_downlink_check(void)
{
    if (s_sym_net_config.dl_mode == (enum ll_downlink_mode)(DOWNLINK_MODE_MAILBOX))
    {
        if ((s_mailbox_check_inhibit == false) && (s_current_state == SYM_READY) &&
            (s_sym_task_config.mailbox_check_int_s != SYM_MANUAL_MAILBOX_CHECK))
        {
            if ((_sym_time_to_now_ms(s_last_dl_check_tick) >
                (s_sym_task_config.mailbox_check_int_s * 1000)) ||
                (s_mailbox_check_next))
            {
                sym_enqueue_cmd(SYM_CMD_CHECK_MAILBOX);
                s_last_dl_check_tick = xTaskGetTickCount();
                
                s_mailbox_check_next = false;
            }
        }
    }
}
/*****************************************************************************/
static bool isTimeToHopGW(void)
{
    uint32_t hop_interval_ms =  s_sym_net_config.gw_hop_interval_s*1000;
    if (_sym_time_to_now_ms(s_gw_hop_tick) > hop_interval_ms)
    {
        return true;
    }
    return false;
}

/*****************************************************************************/
static inline bool _get_io0_state(void)
{
    if (s_sym_task_config.sym_get_io0_state)
    {
        return (s_sym_task_config.sym_get_io0_state(s_sym_task_config.context));
    }
    else
    {
        return (true);
    }
}
/*****************************************************************************/
static inline void _set_power(sym_power_t state)
{
    if (s_sym_task_config.sym_set_power)
    {
        s_sym_task_config.sym_set_power(state, s_sym_task_config.context);
    }
}
/*****************************************************************************/
static inline void _set_boot(sym_boot_t state)
{
    if (s_sym_task_config.sym_set_boot)
    {
        s_sym_task_config.sym_set_boot(state, s_sym_task_config.context);
    }
}
/*****************************************************************************/
static inline void _set_reset(sym_reset_t state)
{
    if (s_sym_task_config.sym_set_reset)
    {
        s_sym_task_config.sym_set_reset(state, s_sym_task_config.context);
    }
}
/*****************************************************************************/
static inline void _on_rx_done(uint8_t *msg, uint16_t len, uint8_t port)
{
    // If we are in mailbox mode and received something, check again
    if (s_sym_net_config.dl_mode == (enum ll_downlink_mode)(DOWNLINK_MODE_MAILBOX))
    {
        s_mailbox_check_next = true;    
    }
    
    if (s_sym_task_config.sym_on_rx_done)
    {
        s_sym_task_config.sym_on_rx_done(msg, len, port, s_sym_task_config.context);
    }
}
/*****************************************************************************/
static inline void _on_suspend(void)
{
    if (s_sym_task_config.sym_on_suspend)
    {
        s_sym_task_config.sym_on_suspend(s_sym_task_config.context);
    }
}
/*****************************************************************************/
static inline void _on_resume(void)
{
    if (s_sym_task_config.sym_on_resume)
    {
        s_sym_task_config.sym_on_resume(s_sym_task_config.context);
    }
}
/*****************************************************************************/
static inline void _on_state_change(sym_module_state_t new_state, sym_module_state_t old_state)
{
    
    if (s_sym_task_config.sym_on_state_change)
    {
        s_sym_task_config.sym_on_state_change(new_state, old_state, s_sym_task_config.context);
    }
}
/*****************************************************************************/
static inline void _on_error(sym_err_code_t err)
{
    if (s_sym_task_config.sym_on_error)
    {
        s_sym_task_config.sym_on_error((uint8_t)(err), s_sym_task_config.context);
    }
}
/*****************************************************************************/
static inline void _on_tx_done(sym_tx_state_t tx_state)
{
    if (s_sym_task_config.sym_on_tx_done)
    {
        s_sym_task_config.sym_on_tx_done(tx_state);
    }
}
/*****************************************************************************/
static inline void _on_send_queue_empty(void)
{
    if (s_sym_task_config.sym_on_ready_idle)
    {
        s_sym_task_config.sym_on_ready_idle(s_sym_task_config.context);
    }
}


/*****************************************************************************/
static bool sym_check_support_hop(void)
{
     bool isSupported = true;
     if (s_sym_version.major < supportHop_version.major)
     {
         isSupported = false;
     }
     else if (s_sym_version.minor<supportHop_version.minor)
     {
         isSupported = false;
     }

     return isSupported;
}

static void sym_delete_gw(uint8_t gw_num)
{
    uint8_t i;
    if (gw_num > (MAX_GW_SCAN_RESULTS-1))
    {
       return;  //invalid gw number
    }
    SYM_DEBUG_PRINTF("Delete Gateway #%d ch: %d from list", gw_num,s_sym_net_config.gw_list[gw_num].gw_data.channel); 

    //erase the lowest and move all down
    for (i=gw_num; i<MAX_GW_SCAN_RESULTS-2; i++)
    {
       s_sym_net_config.gw_list[i].gw_data.is_active = s_sym_net_config.gw_list[i+1].gw_data.is_active;
       s_sym_net_config.gw_list[i].gw_data.id = s_sym_net_config.gw_list[i+1].gw_data.id;
       s_sym_net_config.gw_list[i].gw_data.channel = s_sym_net_config.gw_list[i+1].gw_data.channel;
       s_sym_net_config.gw_list[i].gw_data.error_count = s_sym_net_config.gw_list[i+1].gw_data.error_count;
       s_sym_net_config.gw_list[i].gw_data.rssi = s_sym_net_config.gw_list[i+1].gw_data.rssi;
    }
    
    //reset the last position
    s_sym_net_config.gw_list[i].gw_data.is_active = false;
    s_sym_net_config.gw_list[i].gw_data.id = 0;
    s_sym_net_config.gw_list[i].gw_data.channel = -1;
    s_sym_net_config.gw_list[i].gw_data.error_count = 0;
    s_sym_net_config.gw_list[i].gw_data.rssi = 0;

    if (s_sym_net_config.num_gateways>0)
    {
        s_sym_net_config.num_gateways--;
    }
    
    if (s_sym_task_config.sym_gw_list_update!=NULL)
    {
        s_sym_task_config.sym_gw_list_update(&s_sym_net_config);
    }
}

/***********************************************************************
* if we have gateways above the threshold then we have good gateways.
* delete the bad.
*
*  if we have no good gateways then keep everything in the list.
*
*************************************************************************
*/
static void sym_gw_list_sort(void)
{
    bool isgoodgw = false;

    uint16_t i;
    for (i=0; i<s_sym_net_config.num_gateways; i++)
    {
        if (s_sym_net_config.gw_list[i].gw_results.rssi >= s_sym_net_config.rssi_thresh)
        {
            isgoodgw = true;
            break;
        }
    }

    if (isgoodgw == true)
    {
        for (i=0; i<s_sym_net_config.num_gateways;)
        {
            if (s_sym_net_config.gw_list[i].gw_results.rssi < s_sym_net_config.rssi_thresh)
            {
                sym_delete_gw(i);
            }
            else
            {
                i++;
            }
        }
    }

}
static void sym_change_active_gw(uint8_t new_gw)
{
   uint8_t i;
   for (i=0;i<MAX_GW_SCAN_RESULTS;i++)
   {
       s_sym_net_config.gw_list[i].gw_data.is_active = false;
   }
    
    s_sym_net_config.gw_list[new_gw].gw_data.is_active = true;
}

static int8_t sym_get_active_gw(void)
{
    int8_t i;

    for (i=0; i<MAX_GW_SCAN_RESULTS; i++)
    {
        if (s_sym_net_config.gw_list[i].gw_data.is_active == true)
        {
            return i;
        }
    }
    
    return -1;
}

static bool sym_update_scan_time(void)
{
    s_sym_task_config.sym_get_sys_time(&s_sym_net_time);
    s_sym_net_config.gw_last_info_scan_time_s = s_sym_net_time.seconds;
}

static bool sym_check_info_scan_time(void)
{
    int32_t ret;
    uint32_t elapsed_seconds;

    s_sym_task_config.sym_get_sys_time(&s_sym_net_time);  //get the time from symble

    if (s_sym_net_config.gw_last_info_scan_time_s > s_sym_net_time.seconds)
    {
        // we have an issue.  dont rely on the time. reset scan time?
        s_sym_net_config.gw_last_info_scan_time_s = s_sym_net_time.seconds;
        return false;  
    }

    elapsed_seconds = s_sym_net_time.seconds - s_sym_net_config.gw_last_info_scan_time_s;

    SYM_DEBUG_PRINTF("GWH: scan Time: curr, Last, elapsed, interval: %d, %d, %d, %d", s_sym_net_time.seconds,
                                          s_sym_net_config.gw_last_info_scan_time_s,
                                          elapsed_seconds,
                                          s_sym_net_config.gw_info_scan_interval_s);
       
    if (elapsed_seconds > s_sym_net_config.gw_info_scan_interval_s)
    {
        return true;
    }

    return false; //not time yet
}

/*****************************************************************************/
static sym_module_state_t _sym_reset_state(void)
{
    int32_t ret;
    uint8_t i;
    uint32_t post_reset_delay_ms;

    // config module pins
    _set_power(SYM_POWER_ON);
    _set_boot(SYM_NORMAL_MODE);
    
    _set_reset(SYM_IN_RESET);
    vTaskDelay(pdMS_TO_TICKS(SYM_RESET_LOW_MS));
    wdg_task_refresh(s_sym_wdg_handler);
    _set_reset(SYM_NOT_IN_RESET);
    
    // Use long delay to refresh watchdog
    post_reset_delay_ms = genRandomBetween(1,5);
    post_reset_delay_ms *= 10000;
   // _sym_long_delay(SYM_POST_RESET_DELAY_MS);
    _sym_long_delay(post_reset_delay_ms);

    wdg_task_refresh(s_sym_wdg_handler);
    
    if (++s_clear_flash_count >= IFC_CLEAR_FLASH)
    {
        ll_settings_delete();
        
        _sym_long_delay(SYM_POST_DELETE_DELAY_MS);
        wdg_task_refresh(s_sym_wdg_handler);
        s_clear_flash_count = 0;
    }
    
    // The first time we call this after reset, it fails - call it twice
    for (i=0; i<3; i++)
    {
        // get the module version for future upgrade capability
        ret = ll_version_get(&s_sym_version);
        vTaskDelay(pdMS_TO_TICKS(500u));
    }

    if (ret < 0)
    {
        // Handle this error!   
        return (SYM_STATE_ERROR);
    }
    
    SYM_DEBUG_PRINTF(
        "TEST: Symphony Module:FW= %d.%d.%d", 
        s_sym_version.major, 
        s_sym_version.minor,
        s_sym_version.tag
    );
    
     vTaskDelay(pdMS_TO_TICKS(50u)); //allow module print

    s_does_support_hop = sym_check_support_hop();
    
    // Get the module UUID and store it locally

    ret = ll_unique_id_get(&s_mod_uuid);
    if (ret < 0)
    {
        // Handle this error!   
        return (SYM_STATE_ERROR);
    }
    
    s_err_count = 0;
    s_gw_err_count = 0;
    s_tx_in_progress = false;
    return (SYM_INITIALIZING);
}
/*****************************************************************************/
static sym_module_state_t _sym_initializing(void)
{
    sym_module_state_t next_state = SYM_INITIALIZING;
    err_check_ret_t err;
    int32_t ret;
    uint8_t i;

    do
    {

        _sym_check_reset_msg_state();

        s_sym_state = LL_STATE_INITIALIZING;
        ret = ll_mac_mode_set(SYMPHONY_LINK);
        err = _sym_ll_ifc_error_check(ret);
        if (ERR_RESET == err)
        {
            // The device should be reset under this condition
            next_state = SYM_STATE_ERROR;
        }

        if (NO_ERR != err)
        {
            break;
        }

        if (s_does_support_hop)
        {
            ret = ll_scan_config_set(s_sym_net_config.scan_mode, s_sym_net_config.rssi_thresh_min, s_sym_net_config.scan_attempts);
            SYM_DEBUG_PRINTF("GWH: Setting Mode and Threshold:  %d : %d", s_sym_net_config.scan_mode,  s_sym_net_config.rssi_thresh_min);
            err = _sym_ll_ifc_error_check(ret);
            if (ERR_RESET == err)
            {
                // The device should be reset under this condition
                next_state = SYM_STATE_ERROR;
            }
        
            if (NO_ERR != err)
            {
                break;
            }
        }

        ret = ll_config_set(s_sym_net_config.net_token, s_sym_net_config.app_token,
            s_sym_net_config.dl_mode, s_sym_net_config.qos);  // JJB - network token set will start a scan
        err = _sym_ll_ifc_error_check(ret);
        if (ERR_RESET == err)
        {
            // The device should be reset under this condition
            next_state = SYM_STATE_ERROR;
        }
        
        if (NO_ERR != err)
        {
            break;
        }
        
        // Set downlink band config
        ret = ll_dl_band_cfg_set(&s_sym_net_config.band_cfg); // JJB - this will start a scan
        err = _sym_ll_ifc_error_check(ret);
        if (ERR_RESET == err)
        {
            // The device should be reset under this condition
            next_state = SYM_STATE_ERROR;
        }

        if (NO_ERR != err)
        {
            break;
        }
        
        // Set the time sync mode
        ll_system_time_sync(s_sym_net_config.sync_mode);
        
//        (void) ll_sleep_block();
        
        if (NO_ERR != err)
        {
            break;
        }
       
        // TODO:  If sym module does a scan after reset and we cannot stop it then
        //        we do need to goto Info Scanning, not hopping, to keep things in
        //        sync.  
        //
        // if symphony module version supports hopping and we are in hopping
        // mode then, like it or not, the module is doing an info scan.
        //
        next_state = SYM_SCANNING;
        if ((s_does_support_hop)&&(s_sym_net_config.scan_mode == LLABS_INFO_SCAN))
        {
            next_state = SYM_HOP_GW;
            hop_state = HOP_STATE_INIT;
        }

        s_num_gw_connect_errors = 0;

        for (i=0; i<s_sym_net_config.num_gateways; i++)
        {
            SYM_DEBUG_PRINTF("GWH: gateway #%d , id: 0x%X, channel: %d, RSSI: %d", i, 
                               s_sym_net_config.gw_list[i].gw_results.id,
                               s_sym_net_config.gw_list[i].gw_results.channel,
                               s_sym_net_config.gw_list[i].gw_results.rssi);
        }


    } while (0);

    _sym_get_module_status(true); // clear the IRQ flags
    return (next_state);
}
/*****************************************************************************/
static sym_module_state_t _sym_module_hop_gw(void)
{
    sym_module_state_t next_state = SYM_HOP_GW;
    uint8_t gw_num = 0;
    int32_t ret;
    int8_t curr_gw;
    bool doloop;
    bool is_time;


     switch (hop_state)
     {
        case HOP_STATE_INIT:
          ll_disconnect(); // this will set module in IDLE NO GATEWAY
          vTaskDelay(pdMS_TO_TICKS(100));

          is_time = sym_check_info_scan_time();

          if ((is_time == true)||(s_sym_net_config.num_gateways==0))
          {
              next_state = SYM_INFO_SCANNING;
          }
          else
          {
              hop_state = HOP_STATE_CONNECT;
          }
          
          break;
        case HOP_STATE_CONNECT:
          // wait for disconnect and select a channel
          if (NO_ERR == _sym_get_module_status(false))
          {
              switch (s_sym_state)
              {
                  case LL_STATE_IDLE_CONNECTED:
                      // this shouldnt happen.  If the module is in info scan, it should goto disconnect
                      // after a scan
                       ll_disconnect(); // this will set module in IDLE NO GATEWAY
                       vTaskDelay(pdMS_TO_TICKS(100));
                       _sym_get_module_status(true); // clearing flags
                      break;
                  case LL_STATE_INITIALIZING:
                  case LL_STATE_IDLE_DISCONNECTED:
                      ret = ll_scan_mode_set(LLABS_NORMAL_SCAN_AND_CONNECT);
                      if (ret == LL_IFC_ACK)
                      {
                         doloop = false;
                         curr_gw = sym_get_active_gw();

                         // 
                         // if we have only one gateway then we dont have a choice
                         // pick the same gateway unit it is deleted.
                         //
                         // if no active gateway then dont need to loop just find one.
                         //
                         // If the last active gateway had an error but not deleted yet,
                         // then randomly pick from the list but dont pick the current 
                         // gateway that had an error.  Keep looping until a different
                         // gateway is picked.
                         //
                         if ((s_sym_net_config.num_gateways > 1) && (was_last_gw_error))
                         {
                            doloop= true;
                         }

                         was_last_gw_error = false;

                         if (s_sym_net_config.num_gateways == 0)
                         {
                             next_state = SYM_INFO_SCANNING;
                             break;
                         }

                         do
                         {
                             gw_num = genRandomBetween(0, s_sym_net_config.num_gateways-1);
                             SYM_DEBUG_PRINTF("GWH:%d %d %d", doloop, gw_num, curr_gw);
                         } while ((doloop==true)&&(gw_num == curr_gw));
      
                         if (s_sym_net_config.gw_list[gw_num].gw_data.channel != -1)
                         {
                             
                             ret = ll_connect_to_gw_channel(s_sym_net_config.gw_list[gw_num].gw_data.channel);
                             if (ret == LL_IFC_ACK)
                             {
                                 s_gw_hop_tick =  xTaskGetTickCount();
                                 sym_change_active_gw(gw_num);
                                 next_state = SYM_SCANNING;
                                 SYM_DEBUG_PRINTF("GWH: Hop to #%d[%d] ch: %d", gw_num, s_sym_net_config.num_gateways, s_sym_net_config.gw_list[gw_num].gw_data.channel);
                             }
                         }
                         else
                         {
                             sym_delete_gw(gw_num);
                             SYM_DEBUG_PRINTF("GWH: Invalid Channel GW # %d ch: %d", gw_num, s_sym_net_config.gw_list[gw_num].gw_data.channel);
                         }
                      }
                      break;
                  default:
                      _on_error(SYM_MODULE_STATE_ERROR);
                      next_state = SYM_RESET;
                      break;
              }
              
//              if (_sym_time_in_state_ms() > SYM_DISCONNECTED_TIMEOUT_MS)
//              {   
//                  next_state = SYM_NOT_READY;
//              }
          }
          else
          {
             // get module status error
          }
          break;
        default:
          _on_error(SYM_MODULE_STATE_ERROR);
          next_state = SYM_RESET;

          break;
    };
   
    if (next_state != SYM_HOP_GW)
    {
       hop_state = HOP_STATE_INIT;
    }
    return (next_state);
}


/*****************************************************************************/
/*
**  Function  _sym_module_info_scanning()
**
**  V1.6.0  of module could return that it is disconnected or initializing while it is 
**  doing an info scan.  Commands that start a scan:
**         dl_band_cfg
**         network_token_set
**         set_scan_attempts
**         Control Msg - report scan info
**          
**
** get_scan_info will ACK with no payload when scanning.  We could be disconnected or in 
** initializing when scanning.
**
*/ 
static sym_module_state_t _sym_module_info_scanning(void)
{
    sym_module_state_t next_state = SYM_INFO_SCANNING;
    uint32_t timeout;
    int32_t ret;

    switch (i_scan_state)
    {
        case INFO_SCAN_STATE_INIT:
        {
           
             was_last_gw_error = false;
            if (NO_ERR == _sym_get_module_status(false))
            {
                switch (s_sym_state)
                {
                    case LL_STATE_IDLE_CONNECTED:
                        ll_disconnect(); // this will set module in IDLE NO GATEWAY
                        vTaskDelay(pdMS_TO_TICKS(100));
                        break;
                    case LL_STATE_INITIALIZING:
                    case LL_STATE_IDLE_DISCONNECTED:
                        ret = ll_scan_mode_set(LLABS_INFO_SCAN);
                        if (ret == LL_IFC_ACK)
                        {
                             sym_delete_all_gw(); // make sure list is cleaned up
                            i_scan_state = INFO_SCAN_STATE_WAIT;
                            ll_scan_attempts_set(2); // kick off another info scan
                        }
                        break;
                    case LL_STATE_ERROR:
                        SYM_DEBUG_PRINTF("Module is in Error State ????");
                        _on_error(SYM_MODULE_STATE_ERROR);
                        next_state = SYM_RESET;
                        break;
                    default:
                        _on_error(SYM_MODULE_STATE_ERROR);
                        next_state = SYM_RESET;
                        break;
                        
                }
            }
            else
            {
                // this Info State Init should be fast.  If we cant get the module status
                // or get to scan wait then there is an issue
                if (_sym_time_in_state_ms() > 60000)
                {
                    LL_ASSERT(0);
                }
            }    
            break;            
        }
        case INFO_SCAN_STATE_WAIT:
        {
            if (NO_ERR == _sym_get_module_status(false))
            {

                vTaskDelay(pdMS_TO_TICKS(1000));
                timeout = SYM_INFO_SCAN_TIMEOUT_MS;

                switch (s_sym_state)
                {
                    case LL_STATE_IDLE_CONNECTED:
                        //TODO - this shouldnt happen.  If the module is in info scan, it should goto disconnect
                        // after a scan
                        SYM_DEBUG_PRINTF("GWH: Idle Connected in Info Scan????");
                        _on_error(SYM_MODULE_STATE_ERROR);
                        next_state = SYM_RESET;
                        break;
                    case LL_STATE_INITIALIZING:
                        // drop thru
                    case LL_STATE_IDLE_DISCONNECTED:
                        ret = ll_get_gateway_scan_results((llabs_gateway_scan_results_t (*)[MAX_GW_SCAN_RESULTS])&s_sym_net_config.gw_list,
                                                           &s_sym_net_config.num_gateways);
                        ret = -ret;
                        if (ret == LL_IFC_NACK_BUSY_TRY_AGAIN)
                        {
                            SYM_DEBUG_PRINTF("GWH: SYM_INFO_SCANNING _NACK_BUSY");
                            //if still scanning stay in state until done
                        }
                        else if (ret == LL_IFC_NACK_NODATA)
                        {
                            SYM_DEBUG_PRINTF("GWH: SYM_INFO_SCANNING Done no gateways");
                            s_sym_net_config.num_gateways = 0; // not really any gateways
                            ll_scan_attempts_set(2); // kick off another info scan
                            vTaskDelay(pdMS_TO_TICKS(10));
                        }
                        else if (ret == LL_IFC_ACK)
                        {
                            uint16_t i = 0;

                            SYM_DEBUG_PRINTF("GWH: SYM_INFO_SCANNING Done, %ld gateways found", s_sym_net_config.num_gateways);
                    
                            for (i=0; i<s_sym_net_config.num_gateways; i++)
                            {
                                s_sym_net_config.gw_list[i].gw_data.error_count = 0;
                                s_sym_net_config.gw_list[i].gw_data.is_active = false;

                                SYM_DEBUG_PRINTF("GWH: gateway #%d , id: 0x%X, channel: %d, RSSI: %d", i, 
                                                   s_sym_net_config.gw_list[i].gw_results.id,
                                                   s_sym_net_config.gw_list[i].gw_results.channel,
                                                   s_sym_net_config.gw_list[i].gw_results.rssi);
                            }
                            //we have gateways, sort them, save them, and then go connect
                            sym_gw_list_sort();
                            sym_update_scan_time();

                            if (s_sym_task_config.sym_gw_list_update!=NULL)
                            {
                                s_sym_task_config.sym_gw_list_update(&s_sym_net_config);
                            }
                            // need to print the found and sorted gateways for testing
                            //
                            for (i=0; i<s_sym_net_config.num_gateways; i++)
                            {
                                SYM_DEBUG_PRINTF("GWH: Sorted gateway list #%d , id: 0x%X, channel: %d, RSSI: %d", i, 
                                                   s_sym_net_config.gw_list[i].gw_results.id,
                                                   s_sym_net_config.gw_list[i].gw_results.channel,
                                                   s_sym_net_config.gw_list[i].gw_results.rssi);
                            }
                            
                            next_state = SYM_HOP_GW;
                        }

                        if (_sym_time_in_state_ms() > timeout)
                        {
                            next_state = SYM_NOT_READY;
                        }
                        break;
                   case LL_STATE_ERROR:
                        SYM_DEBUG_PRINTF("Module is in Error State ????");
                        _on_error(SYM_MODULE_STATE_ERROR);
                        next_state = SYM_RESET;
                        break;
                   default:
                        _on_error(SYM_MODULE_STATE_ERROR);
                        next_state = SYM_RESET;
                        break;
                }
                // check for commands
                next_state = _sym_process_cmd_queue(next_state);
            }
            else
            {
                // cannot get module status

            }
            break;
        }  // info scan wait state
        default:
            LL_ASSERT(0);
            break;
    }

    if (next_state != SYM_INFO_SCANNING)
    {
       i_scan_state = INFO_SCAN_STATE_INIT;
    }
    return (next_state);
}
/*****************************************************************************/
static sym_module_state_t _sym_module_scanning(void)
{
    sym_module_state_t next_state = SYM_SCANNING;
    uint32_t timeout;
    uint32_t ret;
    err_check_ret_t mod_status;

    mod_status = _sym_get_module_status(true);

    if (NO_ERR == mod_status)
    {
        timeout = SYM_DISCONNECTED_TIMEOUT_MS;

        switch (s_sym_state)
        {
            case LL_STATE_IDLE_CONNECTED:
                s_tx_fail_count = 0;
                next_state = SYM_TEST_TX;
                break;
            case LL_STATE_INITIALIZING:
                timeout = SYM_INITIALIZING_TIMEOUT_MS;
            case LL_STATE_IDLE_DISCONNECTED:
                if (_sym_time_in_state_ms() > timeout)
                {
                    next_state = SYM_NOT_READY;
                    ll_disconnect(); // stop any scanning process
                    vTaskDelay(pdMS_TO_TICKS(100));
                }
                break;
            default:
                _on_error(SYM_MODULE_STATE_ERROR);
                next_state = SYM_RESET;
                break;
        }
        // check for commands
        next_state = _sym_process_cmd_queue(next_state);
    }
    else
    {
        // if scanning and we have an error flag then start the
        // process over
        if (_sym_time_in_state_ms() > timeout)
        {
            next_state = SYM_NOT_READY;
            ll_disconnect(); // stop any scanning process
            vTaskDelay(pdMS_TO_TICKS(100));
        }
    }

    return (next_state);
}
/*****************************************************************************/
static sym_module_state_t _sym_not_ready(void)
{
    sym_module_state_t next_state = SYM_NOT_READY;
    int8_t curr_gw;
    uint32_t timeout;

    timeout = _sym_get_connection_int_ms();
    // if hopping is supported then we are using the gateway list.  If here, then there was
    // an error scanning or from READY state.
    if ((s_does_support_hop)&&(s_sym_net_config.scan_mode == LLABS_INFO_SCAN))
    {
       timeout = SYM_SETTLE_TIMEOUT_MS;
       was_last_gw_error = true;
    }

    if (NO_ERR == _sym_get_module_status(false))
    {
        if (s_sym_state == LL_STATE_IDLE_CONNECTED)
        {
            s_tx_fail_count = 0;
            next_state = SYM_TEST_TX;
        }
        else if (_sym_time_in_state_ms() > timeout)
        {
            // haven't connected in a long time, try again
            _sym_inc_connection_modifier();
            next_state = SYM_RESET;

            if ((s_does_support_hop)&&(s_sym_net_config.scan_mode == LLABS_INFO_SCAN))
            {
                 if (s_sym_net_config.num_gateways!=0)
                 {
                      curr_gw = sym_get_active_gw();
                      if (curr_gw >= 0)
                      {
                          s_sym_net_config.gw_list[curr_gw].gw_data.error_count+=1;

                          SYM_DEBUG_PRINTF("GWH: Set GW Error gw: %d error %d max %d",  
                                                 curr_gw,
                                                 s_sym_net_config.gw_list[curr_gw].gw_data.error_count,
                                                 s_sym_net_config.gw_max_errors);

                          if (s_sym_net_config.gw_list[curr_gw].gw_data.error_count >= s_sym_net_config.gw_max_errors)
                          {
                              sym_delete_gw(curr_gw);
                          }
                      }
                 }
                 next_state = SYM_HOP_GW;
                 s_gw_err_count+=1;
                  /*
                  ** Symphony Module will sometimes get in a state that will not connect.  Its anecdotal, but
                  ** the module recovers after a system reset.  So rather than resetting just the module we
                  ** are adding the assert to reset the system.
                  */
                 if (s_gw_err_count > GW_CONNECT_MAX_ERRS)
                 {
                      next_state = SYM_RESET;
                      s_gw_err_count = 0;
                      LL_ASSERT(0);
                 }

                 
            }
        }
        else
        {
            // module hasn't successfully connected, wait for user to issue reset/reinit command
            next_state = _sym_process_cmd_queue(SYM_NOT_READY);
        }
    }
    return (next_state);
}
/*****************************************************************************/
static sym_module_state_t _sym_ready(void)
{
    sym_module_state_t next_state = s_current_state;
    sym_tx_state_t tx_status;
    // get status of module
    if (NO_ERR == _sym_get_module_status(s_tx_in_progress)) // force poll if tx is in progress
    {
        if (s_sym_state != LL_STATE_IDLE_CONNECTED)
        {
            SYM_DEBUG_PRINT("TX failed (disconnected)!");
            // cancel tx messages/flush tx msg queue
            // Dont Flush, hold data until we reconnect --
            //       _sym_flush_tx_queue();
            // transition to not ready state
            next_state = SYM_NOT_READY;
            ll_disconnect(); 
            if (s_tx_in_progress == true)
            {
                s_sym_msg_change_state(MSG_STATE_WAITING_READY);  //resend
            }
        }
        else
        {
            // check tx status
            tx_status = _sym_process_tx_state();
            if (tx_status == SYM_TX_SUCCESS)
            {
                next_state = SYM_READY;
            }
            // check rx status
            _sym_process_rx_state();
            // determine next state
            if (s_tx_fail_count > SYM_MAX_TX_FAILURES)
            {
                _on_error(SYM_MAX_TX_FAILURES_ERR);
                // flush tx queue
                // Dont Flush, Hold data until we reconnect --
                //       _sym_flush_tx_queue();
                // transition to not ready state
                next_state = SYM_NOT_READY;
                ll_disconnect(); 
                if (s_tx_in_progress == true)
                {
                    s_tx_in_progress = false;
                    s_sym_msg_change_state(MSG_STATE_WAITING_READY);  //resend
                }               
            }
        }

        /*
        **  SYM_NOT_READY will hop but the state change will prompt Symble to do some
        **  work.  
        */
        if (next_state == SYM_READY)
        {
            // check for commands
            next_state = _sym_process_cmd_queue(next_state);
        }
        /*
        ** a command to reset or power down would override hopping
        */
        if (next_state == SYM_READY)
        {
            if((s_does_support_hop) && (s_sym_net_config.scan_mode == LLABS_INFO_SCAN) && 
               (true == isTimeToHopGW()) && (s_sym_tx_buff.msg_state == MSG_STATE_NONE))
            {
                if (s_sym_task_config.sym_check_safe_hop() == true) 
                {
                    s_gw_hop_tick = xTaskGetTickCount();
                    next_state = SYM_HOP_GW;
                }
            }
        }  
    }

    // bookkeeping
    s_connect_int_modifier = 0;
    s_last_ready_tick = xTaskGetTickCount();

    return (next_state);
}
/*****************************************************************************/
static sym_module_state_t _sym_off(void)
{
    return (_sym_process_cmd_queue(SYM_OFF));
}
/*****************************************************************************/
static sym_module_state_t _sym_fota(void)
{
    uint16_t len = sizeof(s_dl_msg_bfr);
    uint8_t port;
    
    ll_retrieve_message(s_dl_msg_bfr, &len, &port, NULL, NULL);
    
    // We have to refresh the watchdog before processing messages - the FOTA processing
    // can take some time
    wdg_task_refresh(s_sym_wdg_handler);
    if (128 == port)
    {
        ll_ftp_msg_process(&s_ftp, s_dl_msg_bfr, len);
    }

    _sym_get_module_status(false);
    return (SYM_FOTA);
}
/*****************************************************************************/
static err_check_ret_t _sym_ll_ifc_error_check(int32_t ifc_ret)
{
    err_check_ret_t rst = NO_ERR;

    if (ifc_ret < 0)
    {
        SYM_DEBUG_PRINTF("ifc_error_check [%ld]", ifc_ret);
        s_err_count++;
        rst = ERR_NO_RESET;
        _on_error(SYM_IFC_ERR);
        
        switch (ifc_ret)
        {
            case -LL_IFC_NACK_APP_TOKEN_REG:
                SYM_DEBUG_PRINT("app token reg error")
                break;
            case LL_IFC_ERROR_CHECKSUM_MISMATCH:
            case LL_IFC_ERROR_HOST_INTERFACE_TIMEOUT:
                SYM_DEBUG_PRINT("re-init UART");
                sym_uart_reinit(NULL);
                break;
            default:
                break;
        }
    }
    else
    {
        s_err_count = 0;
        rst = NO_ERR;
    }

    if (s_err_count > IFC_MAX_ERRS)
    {
        _on_error(SYM_IFC_ERR_RESET);
        rst = ERR_RESET;
    }

    return (rst);
}
/*****************************************************************************/
static err_check_ret_t _sym_get_module_status(bool force)
{
    err_check_ret_t err = NO_ERR;
    uint32_t irq_flags_tmp;

    if (_sym_time_to_now_ms(s_status_update_tick) > SYM_MIN_STATUS_UPDATE_RATE_MS)
    {
        force = true;
    }
    // read module state & IRQ flags
    do
    {
        if (_get_io0_state() || force)
        {
            if (!(force))
            {
                SYM_DEBUG_PRINT("Polling module status (io0)");
            }
 
            force = true; //set to check err

            err = _sym_ll_ifc_error_check(ll_get_state(&s_sym_state, &s_tx_state, &s_rx_state));
            if (NO_ERR != err)
            {
                break;
            }

            SYM_DEBUG_PRINTF("Polling module: Sym Module State %d", s_sym_state);

            vTaskDelay(30); // wait one tick for module to load next msg (if any)

            err = _sym_ll_ifc_error_check(ll_irq_flags(0xFFFFFFFF, &irq_flags_tmp));
            if (NO_ERR != err)
            {
                break;
            }
            // a module reset or WDT reset will return tx success, if we have a reset and not
            // a tx done flag then change the state
            if ((irq_flags_tmp & IRQ_FLAGS_WDOG_RESET) || (irq_flags_tmp & IRQ_FLAGS_RESET))
            {
                s_tx_state = LL_TX_STATE_ERROR;
            }

            s_irq_flags |= irq_flags_tmp;

            if (irq_flags_tmp != 0)
            {
                SYM_DEBUG_PRINTF("IRQ flags: 0x%08X -> 0x%08X", s_irq_flags, irq_flags_tmp);
            }

            // if there was a sync error return error no reset
            if ((irq_flags_tmp&IRQ_FLAGS_SYNC_FAILED)!=0)
            {
                err = ERR_FLAGS;
                break;
            }
            s_status_update_tick = xTaskGetTickCount();
        }
    } while (0);

    // if there are communication issues with the module then reset everything
    //
    if (force)
    {
        if (NO_ERR != err)
        {
            if (s_module_status_error_count++ > 10)
            {
                LL_ASSERT(0);
            }
        }
        else
        {
            s_module_status_error_count = 0;
        }
    }
    
    return (err);
}

static uint8_t s_data_print_buf[SYMPHONY_MAX_PAYLOAD_SIZE];

void symphony_str_pack_data(const char *label, uint8_t *buff, uint8_t *data, uint16_t len)
{
    char data_head[48];
    uint16_t i;
    size_t head_len;

    // Copy the data label into the header
    memset((char *) data_head, '\0', sizeof(data_head));
    sprintf(data_head, "%s: ", label);
    head_len = strlen(data_head);
    
    // Log the data bytes to debug print
    strncpy((char *) buff, data_head, head_len);
    for (i=0; i < len; i++)
    {
        sprintf((char *) &buff[head_len + (i << 1)], "%02X", data[i]);
    }
    
    buff[head_len + (len << 1)] = '\0';
}

/*****************************************************************************/
static sym_tx_state_t _sym_process_tx_state(void)
{
    err_check_ret_t err;
    sym_send_msg_ret_t mret;
    sym_tx_state_t tx_status = SYM_TX_NONE;

    // check for messages to transmit (if not waiting on any messages)
    if (s_tx_in_progress == false)
    {
        if (s_sym_tx_buff.msg_state == MSG_STATE_WAITING_READY)
        {
            SYM_DEBUG_PRINT("Sending message...");

            s_sym_msg_change_state(MSG_STATE_TRANSMITTING);

            symphony_str_pack_data("SYM SEND DATA", s_data_print_buf, s_sym_tx_buff.msg, s_sym_tx_buff.msg_len);
            LOGF_WARNING("%d %s",s_sym_tx_buff.msg_len, s_data_print_buf);

            err = _sym_ll_ifc_error_check(
                ll_message_send(
                    s_sym_tx_buff.msg, 
                    s_sym_tx_buff.msg_len,
                    s_sym_tx_buff.ack, 
                    s_sym_tx_buff.port
                )
            );
            
            SYM_DEBUG_PRINTF(
                "ll_message_send [%d]: @ %p, len=%d, ack=%d, port=%d",
                (int) err,
                s_sym_tx_buff.msg,
                s_sym_tx_buff.msg_len,
                s_sym_tx_buff.ack,
                s_sym_tx_buff.port
            );
            
            if (NO_ERR != err)
            {
                // notify task that message failed
                LOGS_WARNING("Message send failed!");

                // mret = sym_send_msg(msg_data.msg, msg_data.msg_len,msg_data.ack, msg_data.port);
                s_sym_msg_change_state(MSG_STATE_WAITING_READY);  //resend

                if (mret!=SYM_MSG_ENQUEUED)
                {
                     LOGS_WARNING("Message requeue failed!");
                }

                _on_tx_done(SYM_TX_FAILED);
                tx_status = SYM_TX_FAILED;
            }
            else
            {
                SYM_DEBUG_PRINT("Message send success!");
                s_msg_send_tick = xTaskGetTickCount();
                s_tx_in_progress = true;
            }
        }
        else
        {
            //if the queue is empty, this will notify symble task every 5 ms that we are
            // READY_IDLE
            _on_send_queue_empty();
        }
    }
    else
    {
        // check transmit state
        switch (s_tx_state)
        {
            case LL_TX_STATE_ERROR:
                // notify task that message failed
                _on_tx_done(SYM_TX_FAILED);
                tx_status = SYM_TX_FAILED;
                SYM_DEBUG_PRINT("TX failed!");
                s_tx_in_progress = false;
                s_tx_fail_count++;
                s_sym_msg_change_state(MSG_STATE_WAITING_READY);  //resend
                break;
            case LL_TX_STATE_SUCCESS:
                _on_tx_done(SYM_TX_SUCCESS);
                tx_status = SYM_TX_SUCCESS;
                SYM_DEBUG_PRINT("TX success!");
                s_tx_in_progress = false;
                s_tx_fail_count = 0;
                s_sym_msg_change_state(MSG_STATE_NONE);
                break;
            case LL_TX_STATE_TRANSMITTING:
            default:
                SYM_DEBUG_PRINT("Waiting for tx done...");
                // no action--wait until done
                if (_sym_time_to_now_ms(s_msg_send_tick) > SYM_TX_TIMEOUT_MS)
                {
                    // timeout
                    SYM_DEBUG_PRINT("TX timeout!");
                    _on_tx_done(SYM_TX_FAILED);
                    tx_status = SYM_TX_FAILED;
                    s_tx_in_progress = false;
                    s_tx_fail_count++;
                    s_sym_msg_change_state(MSG_STATE_WAITING_READY);  //resend
                }
                break;
        }
    }
    return tx_status;
}
/*****************************************************************************/
static void _sym_process_rx_state(void)
{
    uint8_t port;
    switch (s_rx_state)
    {
        case LL_RX_STATE_RECEIVED_MSG:
            s_dl_msg_len = sizeof(s_dl_msg_bfr);
            while (s_rx_state == LL_RX_STATE_RECEIVED_MSG)
            {
                SYM_DEBUG_PRINT("Reading RX message");
                if (ll_retrieve_message(s_dl_msg_bfr, &s_dl_msg_len, &port, NULL, NULL) >= 0)
                {
                    if (128 == port)
                    {
                        SYM_DEBUG_PRINT("Port 128 RX message");
                        ll_ftp_msg_process(&s_ftp, s_dl_msg_bfr, s_dl_msg_len);
                    }
                    else
                    {
                        _on_rx_done(s_dl_msg_bfr, s_dl_msg_len, port);
                    }
                    vTaskDelay(1); // wait one tick for module to load next msg (if any)
                    _sym_get_module_status(true); // update rx state
                }
                else
                {
                    SYM_DEBUG_PRINT("FAILED Reading RX message");
                    break;
                }
            }
            break;
        default:
            break;
    }
}
/*****************************************************************************/
static sym_module_state_t _sym_process_cmd_queue(sym_module_state_t state_in)
{
   sym_module_state_t next_state = state_in;
   sym_cmd_t cmd;

    if (xQueueReceive(s_sym_cmd_queue, &cmd, 0) == pdTRUE)
    {
        switch (cmd)
        {
            case SYM_CMD_REINIT:
                s_clear_flash_count = IFC_CLEAR_FLASH;
                _sym_flush_tx_queue();
                next_state = SYM_RESET;
                break;
            case SYM_CMD_RESYNC: // placeholder for resync command
            case SYM_CMD_RST:
                _sym_flush_tx_queue();
                next_state = SYM_RESET;
                break;
            case SYM_CMD_CHECK_MAILBOX:
                if (s_current_state == SYM_READY)
                {
                    if (NO_ERR == _sym_ll_ifc_error_check(ll_mailbox_request()))
                    {
                        SYM_DEBUG_PRINT("Mailbox check issued");
                        s_dl_check_timeout_tick =
                            xTaskGetTickCount() + SYM_DL_CHECK_TIMEOUT_MS / portTICK_RATE_MS;
                    }
                }
                break;
            case SYM_CMD_POWERDOWN:
                _set_power(SYM_POWER_OFF);
                _set_boot(SYM_NORMAL_MODE);
                _set_reset(SYM_NOT_IN_RESET);
                _sym_flush_tx_queue();
                next_state = SYM_OFF;
                break;
            default:
                break;
        }
    }
    return (next_state);
}

/*****************************************************************************/
static uint32_t _sym_time_in_state_ms(void)
{
    return ((xTaskGetTickCount() - s_state_entry_tick) * portTICK_RATE_MS);
}

/*****************************************************************************/
static uint32_t _sym_time_to_now_ms(TickType_t t0)
{
    return ((xTaskGetTickCount() - t0) * portTICK_RATE_MS);
}

/*****************************************************************************/
static uint32_t _sym_get_connection_int_ms(void)
{
    uint32_t ret;

    if (s_sym_task_config.max_connect_interval_s < s_sym_task_config.min_connect_interval_s)
    {
        // user mis-configured task, just use min_connect_interval_s
        ret = s_sym_task_config.min_connect_interval_s;
    }
    else
    {
        ret = s_sym_task_config.min_connect_interval_s;
        ret += s_connect_int_modifier;
        if (ret > s_sym_task_config.max_connect_interval_s)
        {
            ret = s_sym_task_config.max_connect_interval_s;
        }
    }

    // convert to ms
    return (ret * 1000);
}

/*****************************************************************************/
static void _sym_inc_connection_modifier(void)
{
    // confirm modifier is not already over limit before incrementing
    if ((s_sym_task_config.min_connect_interval_s + s_connect_int_modifier) <
        s_sym_task_config.max_connect_interval_s)
    {
        s_connect_int_modifier = (s_connect_int_modifier * 2) + 1;
    }
}
/*****************************************************************************/
static void _sym_flush_tx_queue(void)
{
    sym_tx_msg_struct_t msg_data;
    
    // cancel tx messages/flush tx msg queue
    if (s_tx_in_progress == true)
    {
        _on_tx_done(SYM_TX_FAILED);
        s_tx_in_progress = false;
    }

    s_sym_tx_buff.msg_state = MSG_STATE_NONE;
}
/*****************************************************************************/
static portTickType _sym_get_task_delay(void)
{
    portTickType ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
    uint32_t ms_temp;
    // determine appropriate length of time to sleep
    switch (s_current_state)
    {
        case SYM_READY:
            if (s_tx_in_progress)
            {
                ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
            }
            else
            {
                switch (s_sym_net_config.dl_mode)
                {
                    case DOWNLINK_MODE_ALWAYS_ON:
                        ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        break;
                    default:
                        if (s_sym_tx_buff.msg_state != MSG_STATE_NONE)
                        {
                            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        }
                        else if (uxQueueMessagesWaiting(s_sym_cmd_queue) > 0)
                        {
                            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        }
                        else if (s_sym_task_config.mailbox_check_int_s == SYM_MANUAL_MAILBOX_CHECK)
                        {
                            ret = portMAX_DELAY;
                        }
                        else if (_sym_time_to_now_ms(s_last_dl_check_tick) >
                                 (s_sym_task_config.mailbox_check_int_s * 1000))
                        {
                            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        }
                        else if (s_dl_check_timeout_tick > xTaskGetTickCount())
                        {
                            // just initiated a mailbox check, poll more frequently until timeout
                            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        }
                        else
                        {
#if 0                            
                            ms_temp = (s_sym_task_config.mailbox_check_int_s * 1000) -
                                      _sym_time_to_now_ms(s_last_dl_check_tick);
                            ret = (ms_temp / portTICK_PERIOD_MS);
#else
                            // We cannot block for longer than the watchdog check interval
                            ret = SYM_SLOW_TASK_PERIOD_TICKS;
#endif
                        }
                        break;
                }
            }
            break;
        case SYM_NOT_READY:
            if (_sym_time_in_state_ms() > _sym_get_connection_int_ms())
            {
                ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
            }
            else if ((downlink_mode_t)(s_sym_net_config.dl_mode) == DOWNLINK_MODE_ALWAYS_ON)
            {
                ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
            }
            else
            {
                ms_temp = _sym_get_connection_int_ms() - _sym_time_in_state_ms();
                ret = (ms_temp / portTICK_PERIOD_MS);
            }
            break;
        case SYM_OFF:
            ret = portMAX_DELAY;
            break;
        case SYM_FOTA:
            ret = (SYM_FOTA_TASK_RATE_MS / portTICK_PERIOD_MS);
            break;
        case SYM_RESET:
        case SYM_INITIALIZING:
        case SYM_SCANNING:
        default:
            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
            break;
    }
    
#if 0    
    SYM_DEBUG_PRINTF("sym_get_task_delay ticks: %d", ret);
#endif    

    return (ret);
}
// add some randomness to the long delay so that
// APs dont try to all connect at the same time after
// a power cycle.
static void _sym_long_delay(uint32_t total_delay)
{
    uint32_t delay_interval = (WDG_REC_TOUCH_PERIOD_MS - 500u);
    uint32_t delay_remain;
    
    if (total_delay > delay_interval)
    {
        int32_t wdg_delay;
        
        delay_remain = total_delay;
        while (delay_remain > 0)
        {
            wdg_delay = (delay_remain < delay_interval) ? delay_remain : delay_interval;
            
            wdg_task_refresh(s_sym_wdg_handler);
            SYM_DEBUG_PRINTF("long delay: %dms, remain: %dms", wdg_delay, delay_remain);
            vTaskDelay(pdMS_TO_TICKS(wdg_delay));
            delay_remain -= wdg_delay;
        }
    }
    else
    {
        SYM_DEBUG_PRINTF("long delay: %dms", total_delay);
        vTaskDelay(pdMS_TO_TICKS(total_delay));
    }
}

/*****************************************************************************/
/*****************************************************************************/
/*****LL FTP******************************************************************/
static void _sym_cycle_ftp(void)
{
    if (_sym_time_to_now_ms(s_fota_cycle_tick) > 1000)
    {
        ll_ftp_msg_process(&s_ftp, NULL, 0);
        s_fota_cycle_tick = xTaskGetTickCount();
    }

    if ((s_ftp.state != IDLE) && (s_current_state != SYM_FOTA))
    {
        _sym_flush_tx_queue();
        _on_state_change(SYM_FOTA, s_current_state);
        s_current_state = SYM_FOTA;
        memset(&s_ftp_vars, 0, sizeof(sym_ftp_var_t));
    }
    else if ((s_ftp.state == IDLE) && (s_current_state == SYM_FOTA))
    {
        vTaskDelay(SYM_FTP_DONE_TIMEOUT_MS / portTICK_PERIOD_MS);

        _sym_get_module_status(true);
        if (s_sym_state == LL_STATE_IDLE_CONNECTED)
        {
            s_current_state = SYM_READY;
        }
        else
        {
            s_current_state = SYM_NOT_READY;
        }
        _on_state_change(s_current_state, SYM_FOTA);
        s_ftp_config.ftp_done(s_ftp_vars.success, s_ftp_vars.file_id, s_ftp_vars.file_ver,
                              s_ftp_vars.file_sz);
    }
}
/*****************************************************************************/
static ll_ftp_return_code_t _ftp_open_callback(uint32_t file_id, uint32_t file_version,
                                               uint32_t file_size)
{
    sym_ftp_ret_t ret;
    
    s_ftp_vars.file_id = file_id;
    s_ftp_vars.file_ver = file_version;
    s_ftp_vars.file_sz = file_size;
    s_ftp_vars.success = false;
    ret = s_ftp_config.ftp_file_open(file_id, file_version, file_size);
    
    switch (ret)
    {
        case FTP_OK:
            return (LL_FTP_OK);
            break;
        case FTP_WRONG_FILE_ID:
        case FTP_DUPLICATE_VERSION:
            return (LL_FTP_NO_ACTION);
            break;
        case FTP_SIZE_ERROR:
        case FTP_FLASH_ERROR:
        default:
            return (LL_FTP_ERROR);
            break;
    }
}
/*****************************************************************************/
static ll_ftp_return_code_t _ftp_read_callback(uint32_t file_id, uint32_t file_version,
                                               uint32_t offset, uint8_t *payload, uint16_t len)
{
    sym_ftp_ret_t ret;

    ret = s_ftp_config.ftp_file_read(file_id, file_version, offset, payload, len);

    switch (ret)
    {
        case FTP_OK:
            return (LL_FTP_OK);
            break;
        case FTP_WRONG_FILE_ID:
        case FTP_DUPLICATE_VERSION:
            return (LL_FTP_NO_ACTION);
            break;
        case FTP_SIZE_ERROR:
        case FTP_FLASH_ERROR:
        default:
            return (LL_FTP_ERROR);
            break;
    }
}
/*****************************************************************************/
static ll_ftp_return_code_t _ftp_write_callback(uint32_t file_id, uint32_t file_version,
                                                uint32_t offset, uint8_t *payload, uint16_t len)
{
    sym_ftp_ret_t ret;

    ret = s_ftp_config.ftp_file_write(file_id, file_version, offset, payload, len);

    switch (ret)
    {
        case FTP_OK:
            return (LL_FTP_OK);
            break;
        case FTP_WRONG_FILE_ID:
        case FTP_DUPLICATE_VERSION:
        case FTP_WRONG_VERSION:
            return (LL_FTP_NO_ACTION);
            break;
        case FTP_SIZE_ERROR:
        case FTP_FLASH_ERROR:
        default:
            return (LL_FTP_ERROR);
            break;
    }
}
/*****************************************************************************/
static ll_ftp_return_code_t _ftp_close_callback(uint32_t file_id, uint32_t file_version)
{
    if (s_ftp_config.ftp_file_close == NULL)
    {
        return (LL_FTP_OK);
    }
    else if (s_ftp_config.ftp_file_close(file_id, file_version) == FTP_OK)
    {
        return (LL_FTP_OK);
    }
    else
    {
        return (LL_FTP_ERROR);
    }
}
/*****************************************************************************/
static ll_ftp_return_code_t _ftp_apply_callback(uint32_t file_id, uint32_t file_version,
                                                uint32_t file_size)
{
    s_ftp_vars.success = true;
    s_ftp_vars.file_id = file_id;
    s_ftp_vars.file_ver = file_version;
    s_ftp_vars.file_sz = file_size;
    
    return (LL_FTP_OK);
}

/*****************************************************************************/
static ll_ftp_return_code_t _ftp_send_uplink_callback(const uint8_t *buf, uint16_t len, bool acked,
                                                      uint8_t port)
{
    int32_t ret;

    // Add a small delay before sending messages because the server might not be ready
    vTaskDelay(pdMS_TO_TICKS(500));
    ret = ll_message_send((uint8_t *) buf, len, acked, port);
    
    SYM_DEBUG_PRINTF(
        "ftp_send_uplink: type = %d [%d/%d], ACK = %d, ret = %ld", 
        buf[0],
        len,
        port,
        buf[5], 
        ret
    );

    if (ret < 0)
    {
        return (LL_FTP_ERROR);
    }

    return (LL_FTP_OK);
}
/*****************************************************************************/
static ll_ftp_return_code_t _ftp_dl_config_callback(bool downlink_on)
{
    ll_ftp_return_code_t ret = LL_FTP_OK;
    enum ll_downlink_mode dl_mode;
    static enum ll_downlink_mode s_ftp_dl_mode = LL_DL_MAILBOX;
    
    // If config is being set for DL ALWAYS, we are entering the FTP cycle,
    // save the current DL mode so that we restore it at the end
    if (downlink_on)
    {
        s_ftp_dl_mode = s_sym_net_config.dl_mode;
        dl_mode = LL_DL_ALWAYS_ON;
    }
    else
    {
        // downlink_on is false, FTP is finished - restore the previous value
        dl_mode = s_ftp_dl_mode;
        s_sym_net_config.dl_mode = s_ftp_dl_mode;
    }
    
    if (ll_config_set(s_sym_net_config.net_token, s_sym_net_config.app_token, dl_mode,
                  s_sym_net_config.qos) < 0)
    {
        ret = LL_FTP_ERROR;
    }
    
    return (ret);
}
/*****************************************************************************/
/*****************************************************************************/
