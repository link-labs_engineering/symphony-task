#ifndef SYMPHONY_TASK_H
#define SYMPHONY_TASK_H
/*****************************************************************************/
/*****INCLUDES****************************************************************/
#include <stdbool.h>
#include <stdint.h>

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "ll_ifc.h"
#include "ll_ifc_symphony.h"
    
#include "task.h"
/*****DEFINES*****************************************************************/

#define ONE_DAY_SECONDS                 (86400u)

#define SYM_MANUAL_MAILBOX_CHECK        (0xFFFFFFFF) // code to disable automatic mailbox checks

#define LLABS_DL_CHAN_SPACING           (526595u)

#define GW_HOP_INTERVAL_MIN_S           (100u)
#define GW_HOP_INTERVAL_MAX_S           (1382400u) //16 days
#define GW_HOP_INTERVAL_DEFAULT_S       (432000) // 5 days

#define GW_HOP_SCAN_ATTEMPTS_MIN        (2u)
#define GW_HOP_SCAN_ATTEMPTS_MAX        (10u)

#define GW_HOP_RSSI_THRESHOLD_MIN       (-150)
#define GW_HOP_RSSI_THRESHOLD_MAX       (-40)
#define GW_HOP_RSSI_THRESHOLD_DEFAULT   (-90)

#define GW_INFO_SCAN_INTERVAL_MIN_S     (300u)
#define GW_INFO_SCAN_INTERVAL_MAX_S     (4294967u) //49 days
#define GW_INFO_SCAN_INTERVAL_DEFAULT_S (1296000u) //15 days

#define GW_HOP_MAX_ERRORS_MIN           (2u)
#define GW_HOP_MAX_ERRORS_MAX           (50u)

/*****************************************************************************/
/****TYPES********************************************************************/
// module task states
typedef enum {
    SYM_RESET,
    SYM_INITIALIZING,
    SYM_NOT_READY,
    SYM_SCANNING,
    SYM_TEST_TX,
    SYM_READY,
    SYM_OFF,
    SYM_FOTA,
    SYM_INFO_SCANNING,
    SYM_HOP_GW,
    SYM_STATE_ERROR,
    NUM_SYM_STATES
} sym_module_state_t;
// module supervisor commands
typedef enum {
    SYM_CMD_RST,           // resets module (and powers it up, if necessary)
    SYM_CMD_REINIT,        // resets module AND wipes it's flash variables
    SYM_CMD_RESYNC,        // placeholder, currently does same as reset command
    SYM_CMD_CHECK_MAILBOX, // issues a mailbox check, generally performed autonomously by symphony
                           // task
    SYM_CMD_POWERDOWN // cuts power to module (for applications needing extremely low sleep current)
} sym_cmd_t;
// send message return codes
typedef enum {
    SYM_MSG_ENQUEUED,
    SYM_MSG_QUEUE_FULL,
    SYM_MOD_NOT_READY,
    SYM_NULL_MSG,
    SYM_MSG_TOO_LONG
} sym_send_msg_ret_t;

typedef enum {
    FTP_OK,                 // Return when no error
    FTP_WRONG_FILE_ID,      // Return if file ID is not as expected
    FTP_WRONG_VERSION,      // Return if file version is not as expected
    FTP_SIZE_ERROR,         // Return if file size is too large
    FTP_DUPLICATE_VERSION,  // Return if file version is already in memory
    FTP_FLASH_ERROR,        // Return if there was an error accessing flash
} sym_ftp_ret_t;

typedef enum {
    SYM_POWER_ON,
    SYM_POWER_OFF,
} sym_power_t;

typedef enum {
    SYM_BOOT_MODE,
    SYM_NORMAL_MODE,
} sym_boot_t;

typedef enum {
    SYM_NOT_IN_RESET,
    SYM_IN_RESET,
} sym_reset_t;

typedef enum {
    SYM_TX_NONE = 0,
    SYM_TX_SUCCESS = 1,
    SYM_TX_FAILED = 2,
} sym_tx_state_t;

typedef enum {
    // minor errors
    SYM_IFC_ERR,
    SYM_MAX_TX_FAILURES_ERR,

    SYM_MINOR_ERROR_LIMIT,
    // major errors
    SYM_IFC_ERR_RESET,
    SYM_MODULE_STATE_ERROR,
    SYM_TASK_STATE_ERROR
} sym_err_code_t;

// Configuration structs

typedef struct
{
    uint8_t                         app_token[10];
    uint32_t                        net_token;
    uint32_t                        ds_net_token;
    enum                            ll_downlink_mode dl_mode;
    uint8_t                         qos;
    llabs_dl_band_cfg_t             band_cfg;
    uint8_t                         sync_mode;
    uint16_t                        scan_mode;
    int16_t                         rssi_thresh;
    int16_t                         rssi_thresh_min;
    uint8_t                         scan_attempts;
    llabs_gateway_list_data_t       gw_list[MAX_GW_SCAN_RESULTS];
    uint8_t                         num_gateways;
    uint8_t                         gw_max_errors;
    uint32_t                        gw_hop_interval_s;
    uint32_t                        gw_info_scan_interval_s;
    uint32_t                        gw_last_info_scan_time_s;
} network_config_t;

typedef struct
{
    /**
     * The minimum connection interval.  The task will attempt to reconnect
     * to lost gateways at the interval specified by this setting.
     */
    uint32_t min_connect_interval_s;
    /**
     * The maximum connection interval.  After consecutive failures, the task
     * will gradually increase the connection interval until this limit is reached.
     * May be set equal to min_connect_interval_s if a constant interval is desired.
     */
    uint32_t max_connect_interval_s;
    /**
     * The mailbox check interval.  Ignored if Downlink Always On mode
     * is enabled.  Set to SYM_MANUAL_MAILBOX_CHECK to disable automatic
     * mailbox checking.
     */
    uint32_t mailbox_check_int_s;
    /**
    * Called to poll the state of the IO0 pin. This is an
    * <b>optional function</b> and may be set to NULL if not used.  To
    * achieve the lowest power operating state it should be implemented.
    *
    * @param[in] context The user defined context.
    *
    * @return State of module's IO0 pin
    */
    bool (*sym_get_io0_state)(void *context); // set to NULL if IO0 pin not used
    /**
    * Called to gate power to the module.  The UART peripheral should
    * also be enabled/disabled in this function (if used).  This is an
    * <b>optional function</b> and may be set to NULL if not used.
    *
    * @param[in] state The desired module power state.
    *
    * @param[in] context The user defined context.
    */
    void (*sym_set_power)(sym_power_t state, void *context);
    /**
    * Called to control the module reset pin.  This is an <b>optional function</b>,
    * but is highly recommended.  It may be set to NULL if not used.
    *
    * @param[in] state The desired module reset pin state.
    *
    * @param[in] context The user defined context.
    */
    void (*sym_set_reset)(sym_reset_t state, void *context);
    /**
    * Called to control the module boot pin.  This is an <b>optional function</b>,
    * and may be set to NULL if not used.
    *
    * @param[in] state The desired module boot pin state.
    *
    * @param[in] context The user defined context.
    */
    void (*sym_set_boot)(sym_boot_t state, void *context);
    /**
    * Called when a message has been received.  This is an <b>optional function</b>
    * if downlink support is not required.  It may be set to NULL if not used.
    *
    * @param[in] msg A pointer to the message buffer.
    *
    * @param[in] len The length of the message in msg.
    *
    * @param[in] port The port the message was received on.
    *
    * @param[in] context The user defined context.
    */
    void (*sym_on_rx_done)(uint8_t *msg, uint16_t len, uint8_t port, void *context);
    /**
    * Called by the task before yielding.  This is an <b>optional function</b>,
    * and may be set to NULL if not used.  If low power operation is desired,
    * this function may be used to e.g. set pin modes to reduce current.
    *
    * @param[in] context The user defined context.
    */
    void (*sym_on_suspend)(void *context);
    /**
    * Called by the task upon resuming.  This is an <b>optional function</b>,
    * and may be set to NULL if not used.  Suggested usage is to reverse
    * changes made by sym_on_suspend().
    *
    * @param[in] context The user defined context.
    */
    void (*sym_on_resume)(void *context);
    /**
    * Called by the task when the queue is empty and syphony is ready.  
    *  This is used to unblock symble throttling.  Must be called after transmitting
    *  and backhaul send queue is empty.
    *
    * @param[in] context The user defined context.
    */
    void (*sym_on_ready_idle)(void *context);

    /**
    * Called when the task changes states.  This is an <b>optional function</b>,
    * and may be set to NULL if not used.  The task state may also be polled
    * with sym_get_state().
    *
    * @param[in] new_state The new task state.
    *
    * @param[in] old_state The old task state.
    *
    * @param[in] context The user defined context.
    */
    void (*sym_on_state_change)(sym_module_state_t new_state, sym_module_state_t old_state,
                                void *context);
    /**
    * Called when the task detects an error.  This is an <b>optional function</b>,
    * and may be set to NULL if not used.
    *
    * @param[in] err_code The error code (typecast from sym_err_code_t).
    *
    * @param[in] context The user defined context.
    */
    void (*sym_on_error)(uint8_t err_code, void *context);
    
    void (*sym_on_tx_done)(sym_tx_state_t state);
    /**
    * Called after an info scan or any time the gateway list is
    * updated.  This is an <b>optional function</b>,
    * and may be set to NULL if not used.
    *
    * @param[in] net_config structure containing the gateway list
    *
    */

    void (*sym_gw_list_update)(network_config_t *net_config);
    /*
    ** symphony should not hop if we have items is symble queue
    ** or a connection.  This call back will sync hopping
    ** with symble activity
    */
    bool (*sym_check_safe_hop)(void);
    /*
    ** Symphony Task needs the system time for load balancing.
    */
    bool (*sym_get_sys_time)(llabs_time_t * t_info);
    /**
     * The user context passed into all user defined functions.
     */
    void *context;
} sym_task_config_t;

typedef struct
{
    /**
    * Called by the LL FTP engine to open and allocate storage when
    * a new file transfer begins.
    *
    * @param[in] file_id ID of the incoming file
    *
    * @param[in] file_version Version of the incoming file
    *
    * @param[in] file_size Size (in bytes) of the incoming file
    *
    * @return Return code to indicate success/error of operation
    */
    sym_ftp_ret_t (*ftp_file_open)(uint32_t file_id, uint32_t file_version, uint32_t file_size);
    /**
    * Called by the LL FTP engine to read a chunk of file in memory
    *
    * @param[in] file_id ID of the file
    *
    * @param[in] file_version Version of the file
    *
    * @param[in] offset Offset to begin reading file from
    *
    * @param[out] payload Buffer to read requested bytes into
    *
    * @param[in] len Number of bytes to read
    *
    * @return Return code to indicate success/error of operation
    */
    sym_ftp_ret_t (*ftp_file_read)(uint32_t file_id, uint32_t file_version, uint32_t offset,
                                   uint8_t *payload, uint16_t len);
    /**
    * Called by the LL FTP engine to write a chunk of file to memory
    *
    * @param[in] file_id ID of the file
    *
    * @param[in] file_version Version of the file
    *
    * @param[in] offset Offset to begin writing file at
    *
    * @param[in] payload Buffer with data to be written
    *
    * @param[in] len Number of bytes to write
    *
    * @return Return code to indicate success/error of operation
    */
    sym_ftp_ret_t (*ftp_file_write)(uint32_t file_id, uint32_t file_version, uint32_t offset,
                                    uint8_t *payload, uint16_t len);
    /**
    * Called by the LL FTP engine when memory reading/writing is
    * complete.
    *
    * @param[in] file_id ID of the file
    *
    * @param[in] file_version Version of the file
    *
    * @return Return code to indicate success/error of operation
    */
    sym_ftp_ret_t (*ftp_file_close)(uint32_t file_id, uint32_t file_version);
    /**
    * Called by the LL FTP engine when FTP is complete.
    *
    * @param[in] success 'true' if transfer was successful, 'false' otherwise
    *
    * @param[in] file_id ID of the transferred file
    *
    * @param[in] file_version Version of the transferred file
    *
    * @param[in] file_size Size (in bytes) of the transferred file
    */
    void (*ftp_done)(bool success, uint32_t file_id, uint32_t file_version, uint32_t file_size);
} sym_ftp_config_t;
/*****************************************************************************/

/*****************************************************************************/
void sym_task_disable(void);

int32_t sym_init_task(network_config_t *net_config, sym_task_config_t *task_config,
                      sym_ftp_config_t *ftp_config);
void sym_update_net_config(network_config_t *net_config, bool update_module);
void sym_update_scan_config(network_config_t *net_config);
void sym_update_task_config(sym_task_config_t *task_config);
int32_t sym_enqueue_cmd(sym_cmd_t cmd_data);
sym_send_msg_ret_t sym_send_msg(uint8_t *msg, uint16_t msg_len, bool ack, uint8_t port);
xTaskHandle sym_task_get_handle(void);
void sym_reset_connection_interval(void);
ll_version_t sym_get_version(void);
uint64_t sym_get_uuid(void);
sym_module_state_t sym_get_state(void);
void sym_inhibit_mailbox_check(void);
void sym_allow_mailbox_check(void);
portTickType sym_get_last_connection_time(void);
/*****************************************************************************/
/*****************************************************************************/
#endif
