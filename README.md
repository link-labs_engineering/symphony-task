# Symphony Module Task

A control task for Link Labs Symphony Link modules.  

## Dependencies

This task is designed for embedded systems running FreeRTOS.  It has been developed and tested with FreeRTOS version 8.2.3.  Direct-to-task notifications are used, so this software is NOT compatible with FreeRTOS versions prior to v8.2.0.

The task assumes that UART hardware is configured prior to task initialization and kickoff.

## Usage

### Initialization

The symphony task initialization function accepts three structures: the network configuration, task configuration, and ftp configuration structures.  The net and task config structures are required, the ftp config structure is only needed if the Link Labs File Transfer Protocol will be used (e.g. for OTA firmware updates).

Note: see `symphony_task.h` for complete callback function prototypes and structure definitions.

The network config structure contains settings for the Symphony Link module:

+ **app_token** -- A 10-byte application token (obtained from Conductor)
+ **net_token** -- A 4-byte network token (obtained from Conductor or a configured gateway)
+ **dl_mode** -- The module downlink mode, options are: LL_DL_ALWAYS_ON or LL_DL_MAILBOX
+ **qos** -- Quality of service setting: 0 = lowest QOS, 15 = highest QOS

The task config structure contains means for controlling and interacting with the Symphony Task itself.  It also provides the hardware-specific callbacks required for the task to interact with the Symphony module:  

+ **min_connect_interval_s** -- When disconnected, the task will initially attempt to reconnect to a gateway after this length of time (in seconds)
+ **max_connect_interval_s** -- If this value is greater than the min connect interval, the task will gradually increase its connection retry interval each time a connection attempt fails
+ **mailbox_check_int_s** -- The rate (in seconds) at which the task will autonomously request a mailbox check.  Ignored if downlink mode is set to "Always On"
+ **sym_get_io0_state** -- Callback called by the task to poll state of module's IO0 pin.  Required for lowest-power operation, otherwise set to NULL
+ **sym_set_power** -- Called by the task to toggle power to the module.  If the module is always powered set this parameter to NULL
+ **sym_set_reset** -- Called by the task to toggle the module's reset pin
+ **sym_set_boot** -- Called by the task to toggle the module's boot pin.  Optional, set to NULL if unused
+ **sym_on_rx_done** -- Called by the task when a downlink message has been received
+ **sym_on_suspend** -- Called by the task before going inactive.  Use to set pins and UART in low power state, otherwise set to NULL
+ **sym_on_resume** -- Called by the task upon resuming.  Use to re-enable pins and UART, otherwise set to NULL
+ **sym_on_state_change** -- Called by the task when its internal state has changed.  Optional, set to NULL if unused
+ **sym_on_error** -- Called by the task when an error has occurred (used for error logging/debugging).  Optional, set to NULL if unused
+ **context** -- user configurable context passed into all callbacks.  Set to NULL if unused.

The FTP structure contains callbacks for the FTP engine.  Set to NULL if FTP/FOTA is not required.  These callbacks provide the task with an interface to the harware-specific storage resource (e.g. internal flash, external flash/EEPROM)

+ **ftp_file_open** -- Called when beginning file transfer to 'open' the storage resource
+ **ftp_file_read** -- Called by the task to read a file segment
+ **ftp_file_write** -- Called to write a file segment
+ **ftp_file_close** -- Called to 'close' the storage resource.  *Note that this function does NOT necessarily signify a successful transfer*
+ **ftp_done** -- Called when the FTP transfer is complete.  Will indicate transfer success or failure

### Managing Connection

Upon powerup or reset, the module will attempt to connect to a Symphony Link Gateway.  During this time the symphony task state will transition from `SYM_RESET` to `SYM_INITIALIZING` and finally to `SYM_SCANNING`.  After scanning is complete, the task state will transition to either `SYM_READY` or `SYM_NOT_READY`.  In the `SYM_READY` state, uplink messages can be sent and downlink messages will be received.  In the `SYM_NOT_READY` state, the symphony task will periodically attempt to rescan for a gateway, using the `min_connect_interval_s` and `max_connect_interval_s` parameters.  Attempting to send an uplink message while in the `SYM_NOT_READY` state will result in an error.  If the application wishes to asynchronously initiate a gateway rescan, send the symphony task a reset command with `sym_enqueue_cmd(SYM_CMD_RST)`.  Note that it is recommended that the symphony task is allowed to maintain connectivity on its own (controlled by the min and max connection interval settings).

### Sending Uplinks

Assuming the module is connected, an uplink message can be queued for transmit with the `sym_send_msg` function.  Arguments to this function are:

+ **msg** -- A pointer to the message buffer
+ **msg_len** -- Length of the message to be sent (256 byte maximum)
+ **ack** -- True if a gateway acknowledgement is desired
+ **port** -- Uplink port (generally set to 0)
+ **tx_done_cb** -- Callback upon message success/failure

The calling function should check the function return code to ensure the message has been successfully enqueued.

### Receiving Downlinks

Downlinks are received via the `sym_on_rx_done` callback.  In Downlink Always On mode, the module will listen for downlinks automatically every frame (2 seconds).  In Mailbox mode, the symphony task will autonomously send a mailbox check request at the interval set by `mailbox_check_int_s`.  Note that messages can only be received when the module is connected to a gateway.

## Implementation Examples

The following code snippets illustrate Symphony Task implementations on an Atmel SAML21 processor.  

### Hardware-specific Functions

The Symphony Task requires the user to implement several processor-specific functions.  For UART communications, the user must write functions for `transport_read`, `transport_write`, and `gettime`.  Example implementations for the L21 are below.

```
int32_t transport_write(uint8_t *buff, uint16_t len)
{
    // Wait for all bytes to be sent
    while(usart_sync_is_tx_empty(&SYM_UART) == 0);

    io_write(s_uart_io, (const uint8_t *)buff, len);

    return 0;
}

int32_t transport_read(uint8_t *buff, uint16_t len)
{
    uint16_t bytes_received = 0;

    uint32_t start_tick = xTaskGetTickCount();
    uint32_t timeout_val = (500 / portTICK_PERIOD_MS);

    while ((bytes_received < len) && ((xTaskGetTickCount() - start_tick) < timeout_val))
    {
        if(usart_sync_is_rx_not_empty(sym_desc))
        {
	    io_read(&SYM_UART, &buff[bytes_received], 1);
	    bytes_received++;
        }
    }

    return (bytes_received == len) ? 0 : -1;
}

int32_t gettime(struct time *tp)
{
    long int time_tmp = xTaskGetTickCount();
    
    tp->tv_sec = time_tmp/configTICK_RATE_HZ;
    tp->tv_nsec = ((time_tmp%configTICK_RATE_HZ) * portTICK_PERIOD_MS * 1000000);
    
    return(0);
}
```

The user must also implement the callbacks that provide the task access to I/O pins and resources.  An example of a callback for reading the state of the IO0 pin is shown below.

```
bool bsp_rlp_io0(void * context)
{
    return(gpio_get_pin_level(MOD_IO0));
}
```

### Basic Example

This example illustrates how to initialize the task, send uplinks and receive downlinks.

```
.
.
#include "symphony_task.h"
.
.
// Variables
// sym task config -- 30 second min connection interval, 5 min max, 2 min mailbox 
//					  check rate. Hardware-specific callbacks implemented in 
//				      separate bsp file
static sym_task_config_t s_sym_task_cfg = {30,300,120,
                                           bsp_rlp_io0,bsp_rlp_power,bsp_rlp_rst,
                                           bsp_rlp_boot,_on_rx_done,bsp_rlp_sleep,
                                           bsp_rlp_wake,_on_state_change,NULL,NULL};
// symphony network config
static network_config_t  s_sym_net_cfg = {{0,1,2,3,4,5,6,7,8,9}, // app token
						 				  0x00112233,			 // net token
						 				  LL_DL_MAILBOX,  		 // downlink mode
						 				  7};					 // QOS
// sym task state variable
sym_module_state_t s_sym_state;
.
.
.
// Main Function
void main()
{
	.
	.
	.
	// config and initialize UART
	bsp_sym_uart_init(&RLP_USART);							
	
	// initialize symphony task	
	sym_init_task(&s_sym_net_cfg, &s_sym_task_cfg, NULL);	

	// start FreeRTOS scheduler
	vTaskStartScheduler();
	while(1)
	{

	}
}
.
.
.
// App Callbacks for Symphony Task
static void _on_rx_done(uint8_t* msg, uint16_t len, uint8_t port, void * context)
{
	// load message into queue
    xQueueSendToBack(s_dl_msg_queue, msg, 0);
}

static void _on_state_change(sym_module_state_t new_state, 
							 sym_module_state_t old_state, void * context)
{
	// update state variable
    s_sym_state = new_state;
}

static void _on_tx_done(sym_tx_state_t state)
{
	// notify task that uplink is complete
    xTaskNotify(s_main_task_handle,(uint32_t)(state),eSetBits);
}
.
.
.
// Application Task
static void app_task(void *params)
{
	uint32_t tx_state;

	for(;;)
	{
		.
		.
		.
		if(s_sym_state == SYM_READY)
		{
			// can blink an LED or otherwise indicate connection here
			.
			.
			// send message, call '_on_tx_done' when complete
			if(SYM_MSG_ENQUEUED == sym_send_msg(msg_bfr,msg_sz,true,0,_on_tx_done))
		    {
		        xTaskNotifyWait(0,ULONG_MAX,&tx_state, portMAX_DELAY);
		        // can act on tx success or failure indicated in 'tx_state'
		    }
		}
		.
		.
		.
		// check for downlinks in queue
		if(xQueueReceive(s_dl_msg_queue,s_dl_msg_bfr,0) == pdTRUE)
        {
            // downlink received!  Process here
        }	
        .
        .
        .
	}
}
```

### FTP Example

The following snippet builds on the previous example to add the ability to download a firmware file to an external SPI flash using Link Labs FTP.  Note that the Symphony Task suspends normal uplink/downlink behavior while FTP is occurring.

```
.
.
#include "symphony_task.h"
.
.
// Variables
// sym task config -- 30 second min connection interval, 5 min max, 2 min mailbox 
//					  check rate. Hardware-specific callbacks implemented in 
//				      separate bsp file
static sym_task_config_t s_sym_task_cfg = {30,300,120,
                                           bsp_rlp_io0,bsp_rlp_power,bsp_rlp_rst,
                                           bsp_rlp_boot,_on_rx_done,bsp_rlp_sleep,
                                           bsp_rlp_wake,_on_state_change,NULL,NULL};
// symphony network config
static network_config_t  s_sym_net_cfg = {{0,1,2,3,4,5,6,7,8,9}, // app token
						 				  0x00112233,			 // net token
						 				  LL_DL_MAILBOX,  		 // downlink mode
						 				  7};					 // QOS
// sym task state variable
sym_module_state_t s_sym_state;

// variable for saving file info
static struct
{
    uint32_t file_id; 
    uint32_t file_version;
    uint32_t file_size;
} s_file_info;

// FTP callbacks
static sym_ftp_config_t s_ftp_cb = {_fota_file_open,
									_fota_file_read,
									_fota_file_write,
									_fota_file_close,
									_fota_file_done};
.
.
.
// Main Function
void main()
{
	.
	.
	.
	// config and initialize UART
	bsp_sym_uart_init(&RLP_USART);							
	
	// initialize symphony task	
	sym_init_task(&s_sym_net_cfg, &s_sym_task_cfg, &s_ftp_cb);	

	// start FreeRTOS scheduler
	vTaskStartScheduler();
	while(1)
	{

	}
}
.
.
.
// FTP Callbacks
static sym_ftp_ret_t _fota_file_open(uint32_t file_id, uint32_t file_version,
                                     uint32_t file_size)
{
    sym_ftp_ret_t ret = FTP_OK;
    uint32_t i;
    
    // Check if file id matches expected ID(s)
    if(file_id != YOUR_FOTA_FILE_ID)
    {
        ret = FTP_WRONG_FILE_ID;
    }    
    else if(file_size > FW_IMG_SIZE)
    {
    	// file is too large!
        ret = FTP_FLASH_ERROR;
    }
    else
    {
    	// checks passed
    	// save file info
        s_file_info.file_version = file_version;
        s_file_info.file_id = file_id;
        s_file_info.file_size = file_size;
        
        // enable flash
        bsp_port_enable(SPI_PORT);
        spi_flash_release_from_power_down();
        
        // clear image space
        for (i = 0; i < FW_IMG_SIZE; i+=SECTOR_SZ)
        {
            spi_flash_block_erase(FW_IMG_HDR_ADDR + i, SECTOR_ERASE);
        }
    }
    
    return(ret);
}

static sym_ftp_ret_t _fota_file_read(uint32_t file_id,uint32_t file_version,
                                     uint32_t offset,uint8_t *payload,uint16_t len)
{
    sym_ftp_ret_t ret = FTP_OK;
    
    // ensure id and version numbers match expectations
    if(file_id != s_file_info.file_id)
    {
        ret = FTP_WRONG_FILE_ID;
    }
    else if(file_version != s_file_info.file_version)
    {
        ret = FTP_WRONG_VERSION;
    }
    else
    {
    	// read requested chunk of data from flash
        if(len != spi_flash_read_data(payload, FW_IMG_HDR_ADDR + offset, len))
        {
            ret = FTP_FLASH_ERROR;
        }
    }
    
    return(ret);
}

static sym_ftp_ret_t _fota_file_write(uint32_t file_id,uint32_t file_version,
                                      uint32_t offset,uint8_t *payload,uint16_t len)
{
    sym_ftp_ret_t ret = FTP_OK;
    
    // ensure id and version numbers match expectations
    if(file_id != s_file_info.file_id)
    {
        ret = FTP_WRONG_FILE_ID;
    }
    else if(file_version != s_file_info.file_version)
    {
        ret = FTP_WRONG_VERSION;
    }
    else
    {
    	// write data chunk to memory
        spi_flash_wait_till_ready();
        if(len != spi_flash_write_data(payload, FW_IMG_HDR_ADDR + offset, len))
        {
            ret = FTP_FLASH_ERROR;
        }
    }
    
    return(ret);
}    

static sym_ftp_ret_t _fota_file_close(uint32_t file_id, uint32_t file_version)
{
	// done writing to flash--power down memory
    spi_flash_power_down();
    bsp_port_deinit(SPI_PORT);
    
    return(FTP_OK);
}

static void _fota_file_done(bool success, uint32_t file_id, 
                            uint32_t file_version, uint32_t file_size)
{
    if(success)
    {
        // successful download! Reset into bootloader
    }
    else
    {
    	// download failed, take appropriate action
    }
}
```